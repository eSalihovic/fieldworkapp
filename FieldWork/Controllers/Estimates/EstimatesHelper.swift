//
//  EstimatesHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 03/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct Estimate: Decodable {
    let id: Int?
    let estimateNumber: Int?
    let status: String?
    let issuedOn: String?
    let serviceLocationName: String?
    let customerName: String?
    let total: String?
}

struct EstimateStatus {
    static let accepted = "Accepted"
    static let declined = "Declined"
    static let pending  = "Pending"
}

class Estimates {
    
    class func getEstimates(on page: Int, perPage: Int = perPage, completion: @escaping (_ result: [Estimate]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.estimates, page, perPage, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Estimate].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
