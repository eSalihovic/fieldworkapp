//
//  EstimatesVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 23/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class EstimatesVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var estimates: [Estimate]?
    var searchedEstimates = [Estimate]()
    var searching = false
    var sorted = false
    var isFromCustomer = false
    var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        if !isFromCustomer {
            getEstimates(on: currentPage)
        }
        else {
            guard let estimates = estimates, estimates.isEmpty else { return }
            tableView.isHidden = true
        }
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
    
    @IBAction func sortTapped(_ sender: UIButton) {
        var comparator: (Int, Int) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        estimates = estimates?.sorted(by: { comparator($0.estimateNumber ?? 0, $1.estimateNumber ?? 0) })
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
}

extension EstimatesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchedEstimates.count : estimates?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.estimateTableViewCell, for: indexPath) as? EstimateTableViewCell else { return UITableViewCell() }
        
        var estimateNumberString = String()
        var serviceLocation = String()
        var customerName = String()
        var statusString = String()
        var dateString = String()
        
        if searching {
            estimateNumberString = "# \(searchedEstimates[indexPath.row].estimateNumber ?? 0)"
            customerName = searchedEstimates[indexPath.row].customerName ?? noValueString
            serviceLocation = searchedEstimates[indexPath.row].serviceLocationName ?? noValueString
            statusString = searchedEstimates[indexPath.row].status?.capitalized ?? noValueString
            dateString = searchedEstimates[indexPath.row].issuedOn?.convertStringDateToString() ?? noValueString
        }
        else {
            //DO NOT LEAVE IT THIS WAY
            estimateNumberString = "# \(estimates?[indexPath.row].estimateNumber ?? 0)"
            customerName = estimates?[indexPath.row].customerName ?? noValueString
            serviceLocation = estimates?[indexPath.row].serviceLocationName ?? noValueString
            statusString = estimates?[indexPath.row].status?.capitalized ?? noValueString
            dateString = estimates?[indexPath.row].issuedOn?.convertStringDateToString() ?? noValueString
        }
        
        if statusString.lowercased() == EstimateStatus.accepted.lowercased() {
            cell.statusView.backgroundColor = .fieldworkGreen()
            cell.statusLabel.textColor = .fieldworkGreen()
        }
        else if statusString.lowercased() == EstimateStatus.declined.lowercased() {
            cell.statusView.backgroundColor = .fieldworkRed()
            cell.statusLabel.textColor = .fieldworkRed()
        }
        else if statusString.lowercased() == EstimateStatus.pending.lowercased() {
            cell.statusView.backgroundColor = .fieldworkPurple()
            cell.statusLabel.textColor = .fieldworkPurple()
        }
        else {
            cell.statusView.backgroundColor = .lightGray
            cell.statusLabel.textColor = .lightGray
        }
        
        cell.titleLabel.text = estimateNumberString
        cell.locationLabel.text = serviceLocation
        cell.customerLabel.text = customerName
        cell.statusLabel.text = statusString
        cell.dateLabel.text = dateString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension EstimatesVC: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text != emptyString else {
            searching = false
            tableView.reloadData()
            return
        }
        filterContentForSearchText(text)
        searching = true
        tableView.reloadData()
        textField.resignFirstResponder()
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        tableView.reloadData()
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let estimates = estimates else { return }
        searchedEstimates = estimates.filter({( estimate: Estimate) -> Bool in
            let estimateNrString = String(estimate.estimateNumber!)
            return estimateNrString.contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

extension EstimatesVC {
    
    func getEstimates(on page: Int) {
        Spinner.start()
        Estimates.getEstimates(on: currentPage) { (result) in
            guard let estimates = result else {
                Spinner.stop()
                self.tableView.isHidden = true
                return
            }
            
            self.estimates = estimates
            Spinner.stop()
            self.tableView.reloadData()
        }
    }
}
