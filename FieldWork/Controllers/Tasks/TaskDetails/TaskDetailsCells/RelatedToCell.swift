//
//  RelatedToCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 17/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class RelatedToCell: UITableViewCell {
    
    @IBOutlet var objectLabel: UILabel!
    @IBOutlet var searchLabel: UILabel!
}
