//
//  ScheduleCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 17/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class ScheduleCell: UITableViewCell {
    
    @IBOutlet var calendarDateLabel: UILabel!
    @IBOutlet var calendarTimeLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
}
