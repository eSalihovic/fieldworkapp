//
//  TaskInfoCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 17/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TaskInfoCell: UITableViewCell {
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var dueDateLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var assignedToLabel: UILabel!
    @IBOutlet var statusIcon: UIImageView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var remindMeLabel: UILabel!
    
    func updateStatus(status: String) {
        statusLabel.text = status
        if status.lowercased() == TaskStatus.complete {
            statusLabel.textColor = .completeTaskStatus()
            statusIcon.image = #imageLiteral(resourceName: "statusCompleteIcon")
        }
        else if status.lowercased() == TaskStatus.inProgress {
            statusLabel.textColor = .inProgressTaskStatus()
            statusIcon.image = #imageLiteral(resourceName: "statusInProgress")
        }
        else if status.lowercased() == TaskStatus.pending {
            statusLabel.textColor = .pendingTaskStatus()
            statusIcon.image = #imageLiteral(resourceName: "statusPendingIcon")
        }
        else if status.lowercased() == TaskStatus.waitingForReply {
            statusLabel.textColor = .waitingTaskStatus()
            statusIcon.image = #imageLiteral(resourceName: "statusWaitingIcon")
        }
        else {
            statusLabel.textColor = .fieldworkGray()
            statusIcon.image = nil
        }
    }
}
