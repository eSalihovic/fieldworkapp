//
//  TaskDetailsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 16/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TaskDetailsVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var task: Task!
    var users: [User]?
    var isFromMyTasks = false
    var customer: CustomerName?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = task.name
        let rightBarButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(goToAddOrEditTask))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.addOrEditTask,
            let addOrEditTaskVC = segue.destination as? AddOrEditTaskVC {
            addOrEditTaskVC.task = task
            addOrEditTaskVC.isEdit = true
            addOrEditTaskVC.users = users
            addOrEditTaskVC.isFromCustomer = !isFromMyTasks
            addOrEditTaskVC.customer = customer
            addOrEditTaskVC.title = "Edit task"
        }
    }
    
    @objc
    func goToAddOrEditTask() {
        performSegue(withIdentifier: Segue.addOrEditTask, sender: nil)
    }
}

extension TaskDetailsVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0, let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.taskInfoCell) as? TaskInfoCell {
            cell.descriptionLabel.text = task.description
            cell.dueDateLabel.text = task.dueDate?.convertStringDateToString() ?? noValueString
            cell.typeLabel.text = task.taskType?.name ?? noValueString
            cell.assignedToLabel.text = task.assignedTo
            cell.updateStatus(status: task.status ?? noValueString)
            cell.remindMeLabel.text = noValueString
            
            return cell
        }
        else if indexPath.row == 1, let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.relatedToCell) as? RelatedToCell {
            return cell
        }
        else if indexPath.row == 2, let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.scheduleCell) as? ScheduleCell {
            cell.calendarDateLabel.text = task.calendarDate?.convertStringDateToString() ?? noValueString
            cell.calendarTimeLabel.text = task.calendarDate?.timeFromDateStringToString() ?? noValueString
            cell.durationLabel.text = String(task.duration ?? 0)
            
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
}
