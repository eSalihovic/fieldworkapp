//
//  AddOrEditTaskVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 24/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import TextFieldEffects

class AddOrEditTaskVC: UIViewController {
    
    @IBOutlet var customerTF: AkiraTextField!
    @IBOutlet var nameTF: AkiraTextField!
    @IBOutlet var descriptionTF: AkiraTextField!
    @IBOutlet var dueDateTF: AkiraTextField!
    @IBOutlet var assignedToTF: AkiraTextField!
    @IBOutlet var statusTF: AkiraTextField!
    @IBOutlet var taskTypeTF: AkiraTextField!
    @IBOutlet var remindMeTF: AkiraTextField!
    @IBOutlet var relatedToTF: AkiraTextField!
    @IBOutlet var relatedIdTF: AkiraTextField!
    @IBOutlet var calendarDateTF: AkiraTextField!
    @IBOutlet var calendarTimeTF: AkiraTextField!
    @IBOutlet var durationTF: AkiraTextField!
    @IBOutlet var selectUserButton: UIButton!
    
    var task: Task?
    var users: [User]?
    var isEdit = false
    var customer: CustomerName?
    var isFromCustomer = false
    private var datePicker: UIDatePicker?
    private var timePicker: UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(TasksFilterDateCell.dateChanged(datePicker:)), for: .valueChanged)
        dueDateTF.inputView = datePicker
        calendarDateTF.inputView = datePicker
        
        timePicker = UIDatePicker()
        timePicker?.datePickerMode = .time
        timePicker?.addTarget(self, action: #selector(TasksFilterDateCell.dateChanged(datePicker:)), for: .valueChanged)
        calendarTimeTF.inputView = timePicker
        
        if isEdit {
            setupU()
        }
        if isFromCustomer && !isEdit {
            customerTF.text = customer?.nameTitle
            selectUserButton.isUserInteractionEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.customersVC,
            let customersVC = segue.destination as? CustomersVC else { return }
        customersVC.isFromAddTask = true
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if dueDateTF.isFirstResponder {
            dueDateTF.text = dateFormatter.string(from: datePicker.date)
        }
        else if calendarDateTF.isFirstResponder {
            calendarDateTF.text = dateFormatter.string(from: datePicker.date)
        }
        else if calendarTimeTF.isFirstResponder {
            dateFormatter.dateFormat = "HH:mm"
            calendarTimeTF.text = dateFormatter.string(from: datePicker.date)
        }
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = title
//        let rightBarButton = UIBarButtonItem()
//        rightBarButton.image = #imageLiteral(resourceName: "plusIconGreen")
//        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func actionSheet(with title: String?, data: [String], tf: UITextField) {
        let alertVC = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        alertVC.view.tintColor = .fieldworkGreen()
        for row in data {
            let action = UIAlertAction(title: row, style: .default) { (action) in
                tf.text = action.title
            }
            alertVC.addAction(action)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(cancel)
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func assignedToTapped(_ sender: UIButton) {
        if let users = users {
            var userNames = [String]()
            let userName = "%@ %@"
            for user in users {
                userNames.append(String(format: userName, user.firstName!, user.lastName!))
            }
            actionSheet(with: "Assigned to", data: userNames, tf: assignedToTF)
        }
        else {
            actionSheet(with: "Assigned to", data: [""], tf: assignedToTF)
        }
    }
    
    @IBAction func statusTapped(_ sender: UIButton) {
        let statuses = [TaskStatus.complete.capitalized, TaskStatus.pending.capitalized, TaskStatus.inProgress.capitalized, TaskStatus.waitingForReply.capitalized]
        actionSheet(with: "Status", data: statuses, tf: statusTF)
    }
    
    @IBAction func taskTypeTapped(_ sender: UIButton) {
        let taskType = ["Work Order Review", "Invoice Review", "Follow Up Email", "Follow Up Call", "Welcome Phone Call"]
        
        actionSheet(with: "Task type", data: taskType, tf: taskTypeTF)
    }
    
    @IBAction func remindMeTapped(_ sender: UIButton) {
        let remindMe = ["10 min", "30 min", "60 min"]
        actionSheet(with: "Remind me", data: remindMe, tf: remindMeTF)
    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
        submitTask()
    }
}

extension AddOrEditTaskVC {
    
    func setupU() {
        if isFromCustomer {
            customerTF.text = customer?.nameTitle
        }
        else {
            customerTF.text = task?.relatedName
        }
        nameTF.text = task?.name
        descriptionTF.text = task?.description
        dueDateTF.text = task?.dueDate?.convertStringDateToString()
        assignedToTF.text = task?.assignedTo
        statusTF.text = task?.status
        taskTypeTF.text = task?.taskType?.name
        remindMeTF.text = task?.remindMe
        relatedToTF.text = task?.taskRelatableType
        if let id = task?.taskRelatableId {
            relatedIdTF.text = "\(id)"
        }
        calendarDateTF.text = task?.calendarDate?.convertStringDateToString()
        calendarTimeTF.text = task?.calendarDate?.timeFromDateStringToString()
        if let duration = task?.duration {
            durationTF.text = "\(duration) min"
        }
    }
    
    func submitTask() {
        
        let customerName = customerTF.text
        let name = nameTF.text
        let description = descriptionTF.text
        let dueDate = dueDateTF.text
        let assignedTo = assignedToTF.text
        let status = statusTF.text
        let taskTypeName = taskTypeTF.text
        let remindMe = remindMeTF.text
        let relatedTo = relatedToTF.text
        //let relatedId = relatedIdTF.text
        let calendarDate = createDateStringForApi()
        let duration = createDurationTime(from: durationTF.text)
        
        let taskType = TaskType(name: taskTypeName, createdAt: nil, updatedAt: nil)
        
        let task = Task(id: self.task?.id, name: name, status: status, description: description, duration: duration, dueDate: dueDate, calendarDate: calendarDate, assignedTo: assignedTo, assignedToId: customer?.id, taskRelatableId: nil, taskRelatableType: relatedTo, remindMe: remindMe, relatedName: customerName, taskType: taskType)
        
        Spinner.start()
        SubmitTask.postTask(with: task) { (result) in
            guard let _ = result else {
                // Handle error properly
                return
            }
            
            self.handleControllerUpdate(with: task)
        }
    }
    
    private func createDurationTime(from string: String?) -> Int? {
        guard let durationString = string else { return nil }
        let durationArr = durationString.components(separatedBy: " ")
        return Int(durationArr[0])
    }
    
    private func createDateStringForApi() -> String? {
        guard let calendarDate = calendarDateTF.text,
            let calendarTime = calendarTimeTF.text else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let string = calendarDate + calendarTime
        guard let finalDate = dateFormatter.date(from: string) else { return nil }
        return dateFormatter.string(from: finalDate)
    }
    
    private func handleControllerUpdate(with task: Task) {
        DispatchQueue.main.async {
            if self.isEdit {
                if self.isFromCustomer {
                    self.navigationController?.popViewController(animated: true)
                    let taskDetailsVC = self.navigationController?.viewControllers.last as? TaskDetailsVC
                    taskDetailsVC?.task = task
                    taskDetailsVC?.tableView.reloadData()
                    Spinner.stop()
                    guard let parent = taskDetailsVC?.previousViewController as? CustomerTasksVC else { return }
                    parent.tasks?.insert(task, at: 0)
                    guard let grand = parent.previousViewController as? CustomerDetailsContainerVC else { return }
                    grand.childViewControllers.forEach { (vc) in
                        guard let customerDetailsVC = vc as? CustomerDetailsVC else { return }
                        customerDetailsVC.tasks?.insert(task, at: 0)
                    }
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                    let taskDetailsVC = self.navigationController?.viewControllers.last as? TaskDetailsVC
                    taskDetailsVC?.task = task
                    taskDetailsVC?.tableView.reloadData()
                    Spinner.stop()
                    guard let parent = taskDetailsVC?.previousViewController as? TasksVC else { return }
                    parent.childViewControllers.forEach { (vc) in
                        guard let taskListVC = vc as? TaskListVC else { return }
                        taskListVC.tasks?.insert(task, at: 0)
                    }
                    Spinner.stop()
                }
            }
            else {
                if self.isFromCustomer {
                    self.navigationController?.popViewController(animated: true)
                    let customerTasksVC = self.navigationController?.viewControllers.last as? CustomerTasksVC
                    customerTasksVC?.childViewControllers.forEach { (vc) in
                        guard let taskListVC = vc as? TaskListVC else { return }
                        taskListVC.tasks?.insert(task, at: 0)
                        taskListVC.tableView.reloadData()
                    }
                    Spinner.stop()
                    guard let parent = customerTasksVC?.previousViewController as? CustomerDetailsContainerVC else { return }
                    parent.childViewControllers.forEach { (vc) in
                        guard let customerDetailsVC = vc as? CustomerDetailsVC else { return }
                        customerDetailsVC.tasks?.insert(task, at: 0)
                    }
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                    let tasksVC = self.navigationController?.viewControllers.last as? TasksVC
                    tasksVC?.childViewControllers.forEach { (vc) in
                        guard let taskListVC = vc as? TaskListVC else { return }
                        taskListVC.tasks?.insert(task, at: 0)
                        taskListVC.tableView.reloadData()
                    }
                    Spinner.stop()
                }
            }
        }
    }
}
