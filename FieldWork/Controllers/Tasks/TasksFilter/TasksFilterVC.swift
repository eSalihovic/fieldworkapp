//
//  TasksFilterVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 19/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TasksFilterVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var closeButton: UIButton!
    
    var filters = [Filter]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUsers()
    }
    
    @IBAction func applyTapped(_ sender: UIButton) {
        Spinner.start()
        closeButton.sendActions(for: .touchUpInside)
        TaskFilter.submitTaskFilter(query: filters) { (result) in
            guard let tasks = result, let taskListVC = self.parent as? TaskListVC else {
                Spinner.stop()
                return
            }
            taskListVC.tasks = tasks
            taskListVC.tableView.reloadData()
            Spinner.stop()
        }
    }
    
    @IBAction func resetTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Reset search refinements?", message: "This will reset all refinements but retain your keyword.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okay = UIAlertAction(title: "Okay", style: .default) { _  in
            for (x, filter) in self.filters.enumerated() {
                for (y, _) in filter.properties.enumerated() {
                    self.filters[x].properties[y].selected = false
                }
            }
            self.tableView.reloadData()
        }
        alert.view.tintColor = .fieldworkGreen()
        alert.addAction(cancel)
        alert.addAction(okay)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        guard let vc = self.parent as? TaskListVC else { return }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            vc.filterContainerView.layoutIfNeeded()
            vc.filterContainerView.frame = CGRect(x: vc.view.frame.maxX, y: 0, width: self.view.frame.size.width/6*5, height: vc.view.frame.size.height)
                
        }, completion: { _ in
            vc.filterContainerHidden = true
        })
    }
    
    @objc func handleExpand(button: UIButton) {
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in filters[section].properties.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let expanded = filters[section].isExpanded
        filters[section].isExpanded = !expanded
        
        if expanded {
            tableView.deleteRows(at: indexPaths, with: .fade)
        }
        else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
    }
}

extension TasksFilterVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filters[section].isExpanded ? filters[section].properties.count : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        title = "     " + filters[section].filterName
        let button = UIButton(type: .system)
        button.tag = section
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .fieldworkGray()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(TasksFilterVC.handleExpand), for: .touchUpInside)
        
        return button
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return regularCellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return regularCellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.tasksFilterCell) else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            cell.accessoryView = nil
            if filters[indexPath.section].properties[indexPath.row].selected {
                cell.imageView?.image = #imageLiteral(resourceName: "radioboxMarked")
            }
            else {
                cell.imageView?.image = #imageLiteral(resourceName: "radioboxBlank")
            }
        }
        else if indexPath.section == 1 {
            cell.accessoryView = nil
            if filters[indexPath.section].properties[indexPath.row].selected {
                cell.imageView?.image = #imageLiteral(resourceName: "checkboxMarked")
            }
            else {
                cell.imageView?.image = #imageLiteral(resourceName: "checkboxBlank")
            }
        }
        else if indexPath.section == 2 {
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.tasksFilterDateCell) as? TasksFilterDateCell else { return UITableViewCell() }
            cell2.link = self
            cell2.section = 2
            cell2.updateViews(placeholder: "Add new due date")
            return cell2
        }
        else if indexPath.section == 3 {
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.tasksFilterDateCell) as? TasksFilterDateCell else { return UITableViewCell() }
            cell2.link = self
            cell2.section = 3
            cell2.updateViews(placeholder: "Add new calendar date")
            return cell2
        }
        else if indexPath.section == 4 {
            cell.accessoryView = nil
            if filters[indexPath.section].properties[indexPath.row].selected {
                cell.imageView?.image = #imageLiteral(resourceName: "checkboxMarked")
            }
            else {
                cell.imageView?.image = #imageLiteral(resourceName: "checkboxBlank")
            }
        }
        
        cell.imageView?.image = cell.imageView?.image?.withRenderingMode(.alwaysTemplate)
        cell.textLabel?.text = filters[indexPath.section].properties[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            for (y, _) in filters[0].properties.enumerated() {
                self.filters[0].properties[y].selected = false
            }
            filters[0].properties[indexPath.row].selected = true
            tableView.reloadSections([indexPath.section], with: .none)
        }
        else {
            let property = filters[indexPath.section].properties[indexPath.row]
            let selected = property.selected
            filters[indexPath.section].properties[indexPath.row].selected = !selected
        }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}

extension TasksFilterVC {
    
    func dateUpdate(date: Date, section: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        filters[section].properties[0].name = dateFormatter.string(from: date)
    }
    
    func getUsers() {
        Users.getUsers { (result) in
            guard let users = result else { return }
             var tempUsers = [Property]()
            let userString = "%@ %@"
            for user in users {
                guard let name = user.firstName, let lastName = user.lastName,let id = user.id else { continue }
                tempUsers.append(Property(id: id, name: String(format: userString, name, lastName), selected: false))
            }
            
            self.filters = [
                Filter(isExpanded: false, filterName: "Status", properties: [
                    Property(id: 0, name: "Complete", selected: false),
                    Property(id: 0, name: "Pending", selected: false),
                    Property(id: 0, name: "In Progress", selected: false),
                    Property(id: 0, name: "Waiting for Reply", selected: false)
                ]),
                Filter(isExpanded: false, filterName: "Task type", properties: [
                    Property(id: 2, name: "Work Order Review", selected: false),
                    Property(id: 3, name: "Invoice Review", selected: false),
                    Property(id: 53, name: "Follow Up Email", selected: false),
                    Property(id: 41, name: "Follow Up Call", selected: false),
                    Property(id: 1, name: "Welcome Phone Call", selected: false)
                ]),
                Filter(isExpanded: false, filterName: "Due date", properties: [
                    Property(id: 0, name: "", selected: false)
                ]),
                Filter(isExpanded: false, filterName: "Calendar", properties: [
                    Property(id: 0, name: "", selected: false)
                ]),
                Filter(isExpanded: false, filterName: "Assigned to", properties: tempUsers)
            ]
            self.tableView.reloadData()
        }
    }
}
