//
//  TasksFilterModel.swift
//  FieldWork
//
//  Created by Edin Salihovic on 20/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct Filter {
    var isExpanded: Bool
    var filterName: String
    var properties: [Property]
}

struct Property {
    var id: Int
    var name: String
    var selected: Bool
}

class TaskFilter {
    
    class func submitTaskFilter(query: [Filter], completion: @escaping (_ result: [Task]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        
        //"tasks?filter[status]=%@&filter[task_type]=%@&filter[assigned_to]=%@&filter[due_date]=%@&filter[calendar_date]=%@&api_key=%@"
        
        var status = ""
        for property in query[0].properties {
            guard property.selected else { continue }
            status = property.name
        }
        status = status.lowercased().replacingOccurrences(of: " ", with: "%20")
        status.append("&")
        
        var taskType = ""
        for property in query[1].properties {
            guard property.selected else { continue }
            taskType.append(String(property.id))
            taskType.append("&")
        }
        taskType = taskType.lowercased().replacingOccurrences(of: " ", with: "%20")
        if taskType.isEmpty {
            taskType = "&"
        }
        
        var assignedTo = ""
        for property in query[4].properties {
            guard property.selected else { continue }
            assignedTo.append(String(property.id))
            assignedTo.append("&")
        }
        assignedTo = assignedTo.lowercased().replacingOccurrences(of: " ", with: "%20")
        if assignedTo.isEmpty {
            assignedTo = "&"
        }
        
        var dueDate = ""
        for property in query[2].properties {
            dueDate = property.name
        }
        dueDate.append("&")
        
        var calendarDate = ""
        for property in query[3].properties {
            calendarDate = property.name
        }
        calendarDate.append("&")
        
        let apiUrl = String(format: Endpoint.submitTaskFilters, status, taskType, assignedTo, dueDate, calendarDate, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Task].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
