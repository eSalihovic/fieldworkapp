//
//  TasksFilterDateCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 20/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TasksFilterDateCell: UITableViewCell {
    
    @IBOutlet var dateTextField: UITextField!
    private var datePicker: UIDatePicker?
    var section: Int!
    
    weak var link: TasksFilterVC?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(TasksFilterDateCell.dateChanged(datePicker:)), for: .valueChanged)
        dateTextField.inputView = datePicker
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        link?.dateUpdate(date: datePicker.date, section: section)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    func updateViews(placeholder: String) {
        dateTextField.placeholder = placeholder
    }
}
