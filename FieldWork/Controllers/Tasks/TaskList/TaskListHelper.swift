//
//  TaskListHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 14/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

class Tasks {
    
    class func getTasks(on page: Int, perPage: Int = perPage, completion: @escaping (_ result: [Task]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.tasks, page, perPage, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Task].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
