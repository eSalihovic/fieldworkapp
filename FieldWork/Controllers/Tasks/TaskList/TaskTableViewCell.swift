//
//  TasksTableViewCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var dueDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statusImageView.image = nil
        titleLabel.text = emptyString
        descriptionLabel.text = emptyString
        statusLabel.text = emptyString
        dueDateLabel.text = emptyString
    }
}
