//
//  TaskListVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 23/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TaskListVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var filterContainerView: UIView!
    
    var filterContainerHidden = true
    var tasks: [Task]?
    var searchedTasks = [Task]()
    var isFromMyTasks = false
    var searching = false
    var sorted = false
    var currentPage = 1
    var users: [User]?
    var customer: CustomerName?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFilterContainer()
        guard isFromMyTasks else {
            guard let tasks = tasks, tasks.isEmpty else { return }
            tableView.isHidden = true
            return
        }
        
        getTasks(on: currentPage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.taskDetailsVC,
            let taskDetailsVC = segue.destination as? TaskDetailsVC,
            let task = sender as? Task else { return }
        taskDetailsVC.task = task
        taskDetailsVC.users = users
        taskDetailsVC.isFromMyTasks = isFromMyTasks
        taskDetailsVC.customer = customer
    }
    
    private func setupFilterContainer() {
        filterContainerView.frame = CGRect(x: self.view.frame.maxX, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        filterContainerView.layer.cornerRadius = 10
    }
    
    @IBAction func filterTapped(_ sender: UIButton) {
        if filterContainerHidden {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.filterContainerView.layoutIfNeeded()
                self.filterContainerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                
            }, completion: { _ in
                self.filterContainerHidden = false
            })
        }
        else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.filterContainerView.layoutIfNeeded()
                self.filterContainerView.frame = CGRect(x: self.view.frame.maxX, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                
            }, completion: { _ in
                self.filterContainerHidden = true
            })
        }
        //UIApplication.shared.keyWindow?.addSubview(filterContainerView)
    }
    
    @IBAction func sortTapped(_ sender: UIButton) {
        var comparator: (String, String) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        if searching {
            searchedTasks = searchedTasks.sorted(by: { comparator($0.name?.lowercased() ?? emptyString, $1.name?.lowercased() ?? emptyString) })
        }
        else {
            tasks = tasks?.sorted(by: { comparator($0.name?.lowercased() ?? emptyString, $1.name?.lowercased() ?? emptyString) })
        }
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
//        UIView.transition(with: tableView,
//                          duration: 0.35,
//                          options: .transitionCrossDissolve,
//                          animations: { self.tableView.reloadData() })
    }
}

extension TaskListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchedTasks.count : tasks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.taskTableViewCell, for: indexPath) as? TaskTableViewCell else { return UITableViewCell() }
        
        var taskTitle = String()
        var taskDescription = String()
        var status = String()
        var dueDateString = String()
        if searching {
            taskTitle = searchedTasks[indexPath.row].name ?? emptyString
            taskDescription = searchedTasks[indexPath.row].description ?? emptyString
            status = searchedTasks[indexPath.row].status ?? emptyString
            dueDateString = searchedTasks[indexPath.row].dueDate?.convertStringDateToString() ?? emptyString
        }
        else {
            taskTitle = tasks?[indexPath.row].name ?? emptyString
            taskDescription = tasks?[indexPath.row].description ?? emptyString
            status = tasks?[indexPath.row].status ?? emptyString
            dueDateString = tasks?[indexPath.row].dueDate?.convertStringDateToString() ?? emptyString
        }
        cell.titleLabel.text = taskTitle
        cell.descriptionLabel.text = taskDescription
        cell.statusLabel.text = status
        cell.dueDateLabel.text = dueDateString
        if status.lowercased() == TaskStatus.complete {
            cell.statusLabel.textColor = .completeTaskStatus()
            cell.statusImageView.image = #imageLiteral(resourceName: "statusCompleteIcon")
        }
        else if status.lowercased() == TaskStatus.inProgress {
            cell.statusLabel.textColor = .inProgressTaskStatus()
            cell.statusImageView.image = #imageLiteral(resourceName: "statusInProgress")
        }
        else if status.lowercased() == TaskStatus.pending {
            cell.statusLabel.textColor = .pendingTaskStatus()
            cell.statusImageView.image = #imageLiteral(resourceName: "statusPendingIcon")
        }
        else if status.lowercased() == TaskStatus.waitingForReply {
            cell.statusLabel.textColor = .waitingTaskStatus()
            cell.statusImageView.image = #imageLiteral(resourceName: "statusWaitingIcon")
        }
        
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) && currentPage != 0 && isFromMyTasks {
            currentPage += 1
            getTasks(on: currentPage)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let task = tasks?[indexPath.row]
        performSegue(withIdentifier: Segue.taskDetailsVC, sender: task)
    }
}

extension TaskListVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text != emptyString else {
            searching = false
            tableView.reloadData()
            return
        }
        filterContentForSearchText(text)
        searching = true
        tableView.reloadData()
        textField.resignFirstResponder()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        tableView.reloadData()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let allTasks = tasks else { return }
        searchedTasks = allTasks.filter({( task: Task) -> Bool in
            return task.name!.lowercased().contains(searchText.lowercased())
        })
    }
}

extension TaskListVC {
    
    func getTasks(on page: Int) {
        Spinner.start()
        Tasks.getTasks(on: page) { (result) in
            guard let tasks = result else {
                Spinner.stop()
                self.tableView.isHidden = true
                return
            }
            
            if tasks.count < perPage {
                self.currentPage = 0
            }
            
            self.tasks = tasks
            self.tableView.reloadData()
            Spinner.stop()
        }
    }
}
