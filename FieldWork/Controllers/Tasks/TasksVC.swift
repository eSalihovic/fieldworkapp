//
//  TasksVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class TasksVC: UIViewController {
    
    var users: [User]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        getUsers()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.taskListVC,
            let taskListVC = segue.destination as? TaskListVC {
            taskListVC.isFromMyTasks = true
            taskListVC.users = users
        }
        else if segue.identifier == Segue.addOrEditTask,
            let addOrEditTaskVC = segue.destination as? AddOrEditTaskVC {
            addOrEditTaskVC.title = "Add new task"
            addOrEditTaskVC.users = users
        }
    }
    
    private func setupNavBar() {
        //self.navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        // TODO:
    }
}

extension TasksVC {
    
    func getUsers() {
        Users.getUsers { (users) in
            self.users = users
        }
    }
}
