//
//  DashboardVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 10/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkForSignIn()
    }
    
    private func checkForSignIn() {
        if let apiKey = UserDefaults.standard.string(forKey: "APIKey") {
            print(apiKey)
        } else {
            let signInVC = UIViewController.instantiateSignInVC()
            self.present(signInVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
}
