//
//  ViewController.swift
//  FieldWork
//
//  Created by Edin Salihovic on 5/7/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import TextFieldEffects

class SignInVC: UIViewController {
    
    @IBOutlet var emailTextField: AkiraTextField!
    @IBOutlet var passwordTextField: AkiraTextField!
    @IBOutlet var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func signInTapped(_ sender: UIButton) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        if let email = emailTextField.text, let password = passwordTextField.text, email != emptyString, password != emptyString {
            let loginParams = LoginParams(email: email, password: password)
            SignInHandler.signIn(with: loginParams) { (loginData) in
                if let apiKey = loginData?.apiKey, let name = loginData?.name, let userId = loginData?.userId {
                    UserDefaults.standard.set(apiKey, forKey: "APIKey")
                    UserDefaults.standard.set(name, forKey: "userName")
                    UserDefaults.standard.set(email, forKey: "userEmail")
                    UserDefaults.standard.set(userId, forKey: "userId")
                    DispatchQueue.main.async { [weak self] in
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
                else {
                    let alert = UIAlertController(title: "Incorrect email or password", message: nil, preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else {
            let alert = UIAlertController(title: "Missing email or password", message: nil, preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Got it", style: .cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension SignInVC: UITextFieldDelegate {
    
    // UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            passwordTextField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
            signInButton.sendActions(for: .touchUpInside)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
}
