//
//  SignInHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 5/7/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct LoginParams: Codable {
    let email: String
    let password: String
}

struct Login: Decodable {
    let apiKey: String?
    let name: String?
    let userId: Int?
}

class SignInHandler {
    
    class func signIn(with login: LoginParams, completion: @escaping (_ result: Login?) -> Void) {
        guard let url = URL(string: Endpoint.login) else { return }
        
        // Specify this request as being a POST method
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        // Make sure that we include headers specifying that our request's HTTP body
        // will be JSON encoded
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers
        
        // Now let's encode out Post struct into JSON data...
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(login)
            // ... and set our request's HTTP body
            request.httpBody = jsonData
            print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        } catch {
            completion(nil)
        }
        
        ApiService.postData(with: request) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode(Login.self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
