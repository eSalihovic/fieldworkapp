//
//  AgreementsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 23/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class AgreementsVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.agreementsListVC,
            let agreementsListVC = segue.destination as? AgreementsListVC else { return }
        agreementsListVC.isFromLeftMenu = true
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
}
