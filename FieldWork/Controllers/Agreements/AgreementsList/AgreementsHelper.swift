//
//  AgreementsHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

class Agreements {
    
    class func getAgreements(on page: Int, perPage: Int = perPage, completion: @escaping (_ result: [Agreement]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.agreements, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Agreement].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
