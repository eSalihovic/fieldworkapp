//
//  AgreementsListVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class AgreementsListVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var agreements: [Agreement]?
    var searchedAgreements = [Agreement]()
    var isFromLeftMenu = false
    var searching = false
    var sorted = false
    var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromLeftMenu {
            getAgreements(on: currentPage)
        }
        else  {
            guard let agreements = agreements, agreements.isEmpty else { return }
            tableView.isHidden = true
        }
    }
    
    @IBAction func sortTapped(_ sender: UIButton) {
        var comparator: (Int, Int) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        if searching {
            searchedAgreements = searchedAgreements.sorted(by: { comparator($0.id ?? 0, $1.id ?? 0) })
        }
        else {
            agreements = agreements?.sorted(by: { comparator($0.id ?? 0, $1.id ?? 0) })
        }
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
}

extension AgreementsListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchedAgreements.count : agreements?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.agreementTableViewCell, for: indexPath) as? AgreementTableViewCell else { return UITableViewCell() }
        
        var renews = false
        var agreementNumberString = String()
        var customerName = String()
        var serviceLocation = String()
        var dateString = String()
        var status = String()
        
        if searching {
            renews = searchedAgreements[indexPath.row].renews ?? false
            agreementNumberString = "# \(searchedAgreements[indexPath.row].id ?? 0)"
            customerName = searchedAgreements[indexPath.row].customerName ?? emptyString
            serviceLocation = searchedAgreements[indexPath.row].serviceLocationName ?? emptyString
            dateString = searchedAgreements[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
            status = searchedAgreements[indexPath.row].agreementStatus?.capitalized ?? emptyString
        }
        else {
            //DO NOT LEAVE IT THIS WAY
            renews = agreements?[indexPath.row].renews ?? false
            agreementNumberString = "# \(agreements?[indexPath.row].id ?? 0)"
            customerName = agreements?[indexPath.row].customerName ?? emptyString
            serviceLocation = agreements?[indexPath.row].serviceLocationName ?? emptyString
            dateString = agreements?[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
            status = agreements?[indexPath.row].agreementStatus?.capitalized ?? emptyString
        }
        
        cell.titleLabel.text = agreementNumberString
        cell.locationLabel.text = serviceLocation
        cell.customerLabel.text = customerName
        cell.dateLabel.text = dateString
        cell.statusLabel.text = status
        cell.isRenewingImageView.isHidden = !renews
        
        if status.lowercased() == AgreementStatus.active {
            cell.statusLabel.textColor = .fieldworkGreen()
            cell.statusView.backgroundColor = .fieldworkGreen()
        }
        else if status.lowercased() == AgreementStatus.hold {
            cell.statusLabel.textColor = .fieldworkPurple()
            cell.statusView.backgroundColor = .fieldworkPurple()
        }
        else if status.lowercased() == AgreementStatus.pending {
            cell.statusLabel.textColor = .fieldworkYelow()
            cell.statusView.backgroundColor = .fieldworkYelow()
        }
        else if status.lowercased() == AgreementStatus.canceled {
            cell.statusLabel.textColor = .fieldworkRed()
            cell.statusView.backgroundColor = .fieldworkRed()
        }
        else if status.lowercased() == AgreementStatus.expired {
            cell.statusLabel.textColor = .fieldworkGray()
            cell.statusView.backgroundColor = .fieldworkGray()
        }
        
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) && currentPage != 0 && isFromLeftMenu {
            currentPage += 1
            getAgreements(on: currentPage)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension AgreementsListVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text != emptyString else {
            searching = false
            tableView.reloadData()
            return
        }
        filterContentForSearchText(text)
        searching = true
        tableView.reloadData()
        textField.resignFirstResponder()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        tableView.reloadData()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let agreements = agreements else { return }
        searchedAgreements = agreements.filter({( agreement: Agreement) -> Bool in
            let agreementNrString = String(agreement.id!)
            return agreementNrString.contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

extension AgreementsListVC {
    
    func getAgreements(on page: Int) {
        Spinner.start()
        Agreements.getAgreements(on: currentPage) { (result) in
            guard let agreements = result else {
                Spinner.stop()
                self.tableView.isHidden = true
                return
            }
            
            if agreements.count < perPage {
                self.currentPage = 0
            }
            
            self.agreements = agreements
            self.tableView.reloadData()
            Spinner.stop()
        }
    }
}
