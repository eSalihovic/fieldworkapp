//
//  CalendarVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 09/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CalendarVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
}
