//
//  LeftMenuVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 10/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuVC: UIViewController {

    @IBOutlet var userImage: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var userEmailLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        userImage.layer.borderWidth = 1
        userImage.layer.borderColor = UIColor.fieldworkGreen().cgColor
        setUserNameAndEmail()
    }
    
    private func setUserNameAndEmail() {
        if let name = UserDefaults.standard.value(forKey: "userName") as? String {
            usernameLabel.text = name
            usernameLabel.textColor = .fieldworkGreen()
        }
        if let email = UserDefaults.standard.value(forKey: "userEmail") as? String {
            userEmailLabel.text = email
            userEmailLabel.textColor = .fieldworkGreen()
        }
    }
}

extension LeftMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LeftMenu.titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.leftMenuCell, for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 21)
        cell.textLabel?.textColor = .fieldworkGreen()
        cell.textLabel?.text  = LeftMenu.titles[indexPath.row]
        cell.selectionStyle = .none
        cell.imageView?.image = UIImage(named: LeftMenu.images[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            sideMenuViewController?.contentViewController = .instantiateDashboardVC()
            sideMenuViewController?.hideMenuViewController()
            break
        case 1:
            sideMenuViewController?.contentViewController = .instantiateCustomersVC()
            sideMenuViewController?.hideMenuViewController()
            break
        case 2:
            sideMenuViewController?.contentViewController = .instantiateCalendarVC()
            sideMenuViewController?.hideMenuViewController()
        case 3:
            sideMenuViewController?.contentViewController = .instantiateTasksVC()
            sideMenuViewController?.hideMenuViewController()
        case 4:
            sideMenuViewController?.contentViewController = .instantiateEstimatesVC()
            sideMenuViewController?.hideMenuViewController()
        case 5:
            sideMenuViewController?.contentViewController = .instantiateAgreementsVC()
            sideMenuViewController?.hideMenuViewController()
        case 7:
            UserDefaults.standard.set(nil, forKey: "APIKey")
            let signInVC = UIViewController.instantiateSignInVC()
            self.present(signInVC, animated: false, completion: nil)
        default:
            break
        }
    }
}
