//
//  CustomersHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 11/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct Customer: Decodable {
    let id: Int?
    let accountNumber: Int?
    let namePrefix: String?
    let firstName: String?
    let lastName: String?
    let name: String?
    let customerName: String?
    let tags: String?
}

class CustomerName {
    let nameTitle: String
    let id: Int

    init(nameTitle: String, id: Int) {
        self.nameTitle = nameTitle
        self.id = id
    }

    var titleFirstLetter: String {
        return String(self.nameTitle[self.nameTitle.startIndex]).uppercased()
    }
}

class Customers {
    
    class func getCustomers(by page: Int, perPage: Int = perPage, apiKey: String, completion: @escaping (_ result: [Customer]?) -> Void) {
        // generate the url string
        let apiUrl = String(format: Endpoint.customers, page, perPage, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Customer].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
    
    class func searchCustomer(query: String, filter: String, completion: @escaping (_ result: [Customer]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.searchCustomers, query, filter, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([Customer].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
