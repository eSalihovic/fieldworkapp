//
//  CustomerContactsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 12/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerContactsVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var contacts: [Contact]?
    var searchedContacts = [Contact]()
    var sorted = false
    var customerId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        searchController.setupSearchController(with: self)
        if let contacts = contacts, contacts.isEmpty {
            tableView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    private func setupNavBar() {
        //self.navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @IBAction func sortTapped(_ sender: UIBarButtonItem) {
        var comparator: (String, String) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        contacts = contacts?.sorted(by: { comparator($0.lastName?.lowercased() ?? emptyString, $1.lastName?.lowercased() ?? emptyString) })
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.addOrEditContact,
            let addOrEditContactVC = segue.destination as? AddOrEditContactVC {
            addOrEditContactVC.customerId = customerId
        }
    }
}

extension CustomerContactsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching() ? searchedContacts.count : contacts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.contactCell, for: indexPath)
        
        if isSearching() {
            let lastName = searchedContacts[indexPath.row].lastName ?? emptyString
            let firstName = searchedContacts[indexPath.row].firstName ?? emptyString
            cell.textLabel?.text = String(format: contactName, lastName, firstName)
            cell.detailTextLabel?.text = contacts?[indexPath.row].description ?? emptyString
        }
        else {
            let lastName = contacts?[indexPath.row].lastName ?? emptyString
            let firstName = contacts?[indexPath.row].firstName ?? emptyString
            cell.textLabel?.text = String(format: contactName, lastName, firstName)
            cell.detailTextLabel?.text = contacts?[indexPath.row].description ?? emptyString
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CustomerContactsVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        filterContentForSearchText(text)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let contacts = contacts else { return }
        searchedContacts = contacts.filter({( contact: Contact) -> Bool in
            return contact.firstName!.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}
