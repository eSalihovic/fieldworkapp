//
//  ContactPhoneCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 06/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import TextFieldEffects

class ContactPhoneCell: UITableViewCell {
    
    @IBOutlet var phoneTextField: AkiraTextField!
    @IBOutlet var typeTextField: AkiraTextField!
    @IBOutlet var extTextField: JiroTextField!
    @IBOutlet var addOrRemoveButton: UIButton!
    
    weak var delegate: AddOrRemoveContactNumberDelegate?
    var picker: UIPickerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        picker = UIPickerView()
        typeTextField.inputView = picker
        phoneTextField.tag = 1
        typeTextField.tag = 2
        extTextField.tag = 3
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        phoneTextField.text = emptyString
        typeTextField.text = emptyString
        extTextField.text = emptyString
    }
    
    @IBAction func addOrRemoveTapped(_ sender: UIButton) {
        delegate?.addOrRemoveCell(sender.tag)
    }
}
