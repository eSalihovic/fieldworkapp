//
//  AddOrEditContactVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 06/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import TextFieldEffects

protocol AddOrRemoveContactNumberDelegate: class {
    func addOrRemoveCell(_ tag: Int)
}

class AddOrEditContactVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var contactTitleTextField: KaedeTextField!
    @IBOutlet var contactInfoTextField: [AkiraTextField]!
    @IBOutlet var contactInfoUnderline: [UIView]!
    @IBOutlet var emailInvoicesSwitch: UISwitch!
    @IBOutlet var emailWorkOrdersSwitch: UISwitch!
    
    let titleData = ["Dr.", "Mr.", "Ms.", "Mrs."]
    let typeData = ["Home", "Office", "Mobile", "Fax", "Other"]
    var picker = UIPickerView()
    var phones = [Phone()]
    var customerId: Int?
    
    struct Phone {
        var number = String()
        var type = String()
        var ext = String()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        picker.delegate = self
        picker.dataSource = self
        contactTitleTextField.inputView = picker
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = "Add Contact"
    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
        submitContact()
    }
}

extension AddOrEditContactVC: AddOrRemoveContactNumberDelegate {
    func addOrRemoveCell(_ tag: Int) {
        if tag == 0 {
            guard phones.count < 5 else {
                // Handle error properly
                return
            }
            phones.append(Phone())
            tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .none)
        }
        else {
            phones.remove(at: tag)
            tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .none)
        }
    }
}

extension AddOrEditContactVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.contactPhoneCell, for: indexPath) as? ContactPhoneCell else { return UITableViewCell() }
        
        cell.delegate = self
        cell.picker.delegate = self
        cell.picker.dataSource = self
        cell.picker.tag = indexPath.row
        cell.addOrRemoveButton.tag = indexPath.row
        
        cell.phoneTextField.text = phones[indexPath.row].number
        cell.typeTextField.text = phones[indexPath.row].type
        cell.extTextField.text = phones[indexPath.row].ext
        
        if indexPath.row == 0 {
            cell.addOrRemoveButton.setImage(#imageLiteral(resourceName: "plusIconGreen"), for: UIControlState())
        }
        else {
            cell.addOrRemoveButton.setImage(#imageLiteral(resourceName: "minusIconGreen"), for: UIControlState())
            cell.addOrRemoveButton.imageView?.contentMode = .scaleAspectFit
            cell.addOrRemoveButton.imageView?.tintColor = .red
        }
        
        return cell
    }
}

extension AddOrEditContactVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return contactTitleTextField.isFirstResponder ? titleData.count : typeData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if contactTitleTextField.isFirstResponder {
            contactTitleTextField.text = titleData[row]
        }
        else if let cell = tableView.cellForRow(at: IndexPath(row: pickerView.tag, section: 0)) as? ContactPhoneCell {
            cell.typeTextField.text = typeData[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return contactTitleTextField.isFirstResponder ? titleData[row] : typeData[row]
    }
}

extension AddOrEditContactVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tf = textField as? AkiraTextField
        
        guard let text = textField.text, text != emptyString else {
            tf?.placeholderColor = .red
            return
        }
        tf?.placeholderColor = .fieldworkGray()
        
        guard let cell = textField.superview?.superview as? ContactPhoneCell else { return }
        switch textField.tag {
        case 1: phones[cell.picker.tag].number = textField.text ?? emptyString
        case 2: phones[cell.picker.tag].type = textField.text ?? emptyString
        case 3: phones[cell.picker.tag].ext = textField.text ?? emptyString
        default: break
        }
    }
}

extension AddOrEditContactVC {
    
    func handleEmptyTextFieldWarning() {
        for (tfIndex, textfield) in contactInfoTextField.enumerated() {
            guard textfield.text == emptyString else {
                textfield.placeholderColor = .fieldworkGray()
                contactInfoUnderline[tfIndex].backgroundColor = .fieldworkGray()
                continue
            }
            
            textfield.placeholderColor = .red
            contactInfoUnderline[tfIndex].backgroundColor = .red
            tableView.setContentOffset(.zero, animated: true)
        }
        
        guard let cells = tableView.visibleCells  as? [ContactPhoneCell] else { return }
        for cell in cells {
            if cell.phoneTextField.text != emptyString {
                cell.phoneTextField.placeholderColor = .fieldworkGray()
            }
            else {
                cell.phoneTextField.placeholderColor = .red
            }
            if cell.typeTextField.text != emptyString {
                cell.typeTextField.placeholderColor = .fieldworkGray()
            }
            else {
                cell.typeTextField.placeholderColor = .red
            }
        }
    }
    
    func submitContact() {
        guard let customerId = customerId else { return }
        let phone = phones.first?.number ?? emptyString
        let phoneExt = phones.first?.ext ?? emptyString
        let phoneKind = phones.first?.type ?? emptyString
        
        var _phones = [String]()
        var _phoneExts = [String]()
        var _phoneKinds = [String]()
        
        for phone in phones {
            _phones.append(phone.number)
            _phoneExts.append(phone.ext)
            _phoneKinds.append(phone.type)
        }
        
        let optional = Array(_phones.dropFirst())
        let phoneExts = Array(_phoneKinds.dropFirst())
        let phoneKinds = Array(_phoneExts.dropFirst())
        
        let contact = Contact(firstName: contactInfoTextField[0].text,
                              lastName: contactInfoTextField[1].text,
                              title: contactTitleTextField.text,
                              description: contactInfoTextField[3].text,
                              phone: phone,
                              phoneExt: phoneExt,
                              phoneKind: phoneKind,
                              phones: optional,
                              phonesKinds: phoneExts,
                              phonesExts: phoneKinds,
                              email: contactInfoTextField[2].text,
                              emailInvoices: emailInvoicesSwitch.isOn,
                              emailWorkOrders: emailWorkOrdersSwitch.isOn)
        
        guard contact.firstName != emptyString && contact.lastName != emptyString && contact.phone != emptyString && contact.phoneKind != emptyString else {
            // Handle error properly
            handleEmptyTextFieldWarning()
            return
        }
        
        Spinner.start()
        SubmitContact.postContact(with: customerId, contact: contact) { (result) in
            guard let _ = result else {
                // Handle error properly
                return
            }
            
            DispatchQueue.main.async {
                //CustomerContactsVC
                self.navigationController?.popViewController(animated: true)
                let contactVC = self.navigationController?.viewControllers.last as? CustomerContactsVC
                contactVC?.contacts?.insert(contact, at: 0)
                Spinner.stop()
                guard let noteParent = contactVC?.previousViewController as? CustomerDetailsContainerVC else { return }
                noteParent.childViewControllers.forEach { (vc) in
                    guard let customerDetailsVC = vc as? CustomerDetailsVC else { return }
                    customerDetailsVC.contacts?.insert(contact, at: 0)
                }
            }
        }
    }
}
