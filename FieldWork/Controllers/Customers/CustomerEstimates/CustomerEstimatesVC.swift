//
//  CustomerEstimatesVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 26/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerEstimatesVC: UIViewController {
    
    var estimates: [Estimate]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.estimatesVC,
            let estimatesVC = segue.destination as? EstimatesVC else { return }
        estimatesVC.estimates = estimates
        estimatesVC.isFromCustomer = true
    }
}
