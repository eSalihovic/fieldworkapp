//
//  CustomerNotesVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 19/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerNotesVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var notes: [Note]?
    var searchedNotes = [Note]()
    var sorted = false
    var customerId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        searchController.setupSearchController(with: self)
        if let notes = notes, notes.isEmpty {
            tableView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.addOrEditNoteVC,
            let addOrEditNoteVC = segue.destination as? AddOrEditNoteVC {
            addOrEditNoteVC.customerId = customerId
        }
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @IBAction func sortTapped(_ sender: UIBarButtonItem) {
        var comparator: (String, String) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        notes = notes?.sorted(by: { comparator($0.body?.lowercased() ?? emptyString, $1.body?.lowercased() ?? emptyString) })
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
}

extension CustomerNotesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching() ? searchedNotes.count : notes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.noteTableViewCell, for: indexPath) as? NoteTableViewCell else { return UITableViewCell() }
        
        var note = String()
        var dateString = String()
        if isSearching() {
            note = searchedNotes[indexPath.row].body ?? emptyString
            dateString = searchedNotes[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
        }
        else {
            note = notes?[indexPath.row].body ?? emptyString
            dateString = notes?[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
        }
        
        if notes?[indexPath.row].attachmentContentType != nil {
            cell.attachmentIconImageView.image?.withRenderingMode(.alwaysTemplate)
            cell.attachmentIconImageView.image = #imageLiteral(resourceName: "attachmentIcon")
        }
        else {
            cell.attachmentIconImageView.image = nil
        }
        
        cell.noteLabel.text = note
        cell.dateLabel.text = dateString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension CustomerNotesVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        filterContentForSearchText(text)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let notes = notes else { return }
        searchedNotes = notes.filter({( note: Note) -> Bool in
            return note.body!.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}
