//
//  NotesTableViewCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 21/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    
    @IBOutlet var noteLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var attachmentIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        noteLabel.text = emptyString
        dateLabel.text = emptyString
        attachmentIconImageView.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
