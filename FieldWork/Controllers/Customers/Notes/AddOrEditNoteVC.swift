//
//  AddOrEditNoteVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 12/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class AddOrEditNoteVC: UIViewController {
    
    @IBOutlet var noteTextView: UITextView!
    @IBOutlet var noteUnderline: UIView!
    @IBOutlet var bodyLabel: UILabel!
    
    var customerId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
//        textViewDidChange(noteTextView)
        NotificationCenter.default.addObserver(self, selector: #selector(AddOrEditNoteVC.updateTextView(by:)), name: .UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddOrEditNoteVC.updateTextView(by:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        noteTextView.resignFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = "Add Note"
    }
    @IBAction func submitNoteTapped(_ sender: UIButton) {
        submitNote()
    }
}

extension AddOrEditNoteVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        noteUnderline.backgroundColor = .fieldworkGray()
        bodyLabel.textColor = .fieldworkGray()
//        let size = CGSize(width: view.frame.height - 40, height: .infinity)
//        let estimatedSize = textView.sizeThatFits(size)
//        textView.constraints.forEach { (constraint) in
//            guard constraint.firstAttribute == .height else { return }
//            constraint.constant = estimatedSize.height
//        }
    }
}

extension AddOrEditNoteVC {
    
    @objc
    func updateTextView(by notification: Notification) {
        guard let userinfo = notification.userInfo,
        let keyboardEndFrameScreenCoordinates = (userinfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardEndFrame = view.convert(keyboardEndFrameScreenCoordinates, to: view.window)
        if notification.name == .UIKeyboardWillHide {
            noteTextView.contentInset = UIEdgeInsetsMake(0, 0, keyboardEndFrame.height, 0)
            noteTextView.scrollIndicatorInsets = noteTextView.contentInset
        }
        noteTextView.scrollRangeToVisible(noteTextView.selectedRange)
    }
    
    func submitNote() {
        let userId = UserDefaults.standard.value(forKey: "userId") as? Int
        let noteText = noteTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard let customerId = customerId, !noteText.isEmpty else {
            noteUnderline.backgroundColor = .red
            bodyLabel.textColor = .red
            return
        }
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let currentDateString = formatter.string(from: date)
        
        let note = Note(attachmentContentType: nil, body: noteTextView.text, createdAt: currentDateString, userId: userId)
        
        Spinner.start()
        SubmitNote.postNote(with: customerId, note: note) { (result) in
            guard let _ = result else {
                // Handle error properly
                return
            }
            
            DispatchQueue.main.async {
                //CustomerContactsVC
                self.navigationController?.popViewController(animated: true)
                let noteVC = self.navigationController?.viewControllers.last as? CustomerNotesVC
                noteVC?.notes?.insert(note, at: 0)
                Spinner.stop()
                guard let noteParent = noteVC?.previousViewController as? CustomerDetailsContainerVC else { return }
                noteParent.childViewControllers.forEach { (vc) in
                    guard let customerDetailsVC = vc as? CustomerDetailsVC else { return }
                    customerDetailsVC.notes?.insert(note, at: 0)
                }
            }
        }
    }
}
