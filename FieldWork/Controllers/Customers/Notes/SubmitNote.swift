//
//  SubmitNote.swift
//  FieldWork
//
//  Created by Edin Salihovic on 13/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

class SubmitNote {
    
    class func postNote(with customerId: Int, note: Note, completion: @escaping (_ result: Note?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.submitNote, customerId, apiKey)
        // the URL
        guard let url = URL(string: apiUrl) else { return }
        print("url = \(url)")
        
        // Specify this request as being a POST method
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        // Make sure that we include headers specifying that our request's HTTP body
        // will be JSON encoded
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = 100
        
        // Now let's encode out Post struct into JSON data...
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        do {
            let jsonData = try encoder.encode(note)
            // ... and set our request's HTTP body
            request.httpBody = jsonData
            print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        } catch {
            completion(nil)
        }
        
        ApiService.postData(with: request) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode(Note.self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
