//
//  CustomerTasksVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 23/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerTasksVC: UIViewController {
    
    var tasks: [Task]?
    var customer: CustomerName!
    var users: [User]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = "Tasks"
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "plusIconGreen"), style: .plain, target: self, action: #selector(goToAddOrEditTask))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.customerTasks,
            let taskListVC = segue.destination as? TaskListVC {
            taskListVC.customer = customer
            taskListVC.tasks = tasks
            taskListVC.users = users
        }
        else if segue.identifier == Segue.addOrEditTask,
            let addOrEditTaskVC = segue.destination as? AddOrEditTaskVC {
            addOrEditTaskVC.title = "Add new task"
            addOrEditTaskVC.customer = customer
            addOrEditTaskVC.isFromCustomer = true
            addOrEditTaskVC.users = users
        }
    }
    
    @objc
    func goToAddOrEditTask() {
        performSegue(withIdentifier: Segue.addOrEditTask, sender: nil)
    }
}
