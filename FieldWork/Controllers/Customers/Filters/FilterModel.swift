//
//  FilterModel.swift
//  FieldWork
//
//  Created by Edin Salihovic on 21/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct FilterData {
    var expanded = Bool()
    var filterTitle = String()
    var sectionData = [StatusName]()
}

enum StatusName: String {
    case active = "Active"
    case inactive = "Inactive"
    case sendToCollections = "Sent to collections"
    case financialHold = "Financial hold"
}

enum StatusKey: String {
    case active = "active"
    case inactive = "inactive"
    case sendToCollections = "sent_to_collections"
    case financialHold = "financial_hold"
}

class FilterModel {
    
    static let filterData = [FilterData(expanded: false, filterTitle: "Status", sectionData: [.active, .inactive, . sendToCollections, .financialHold])]
}
