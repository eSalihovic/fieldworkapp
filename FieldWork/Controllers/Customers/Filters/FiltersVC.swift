//
//  FiltersVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 21/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class FiltersVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var closeButton: UIButton!
    
    var filterData = FilterModel.filterData
    var statusKey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        guard let vc = self.parent as? CustomersVC else { return }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            vc.filterContainerView.layoutIfNeeded()
            vc.filterContainerView.frame = CGRect(x: vc.view.frame.maxX, y: 0, width: self.view.frame.size.width/6*5, height: vc.view.frame.size.height)
            
        }, completion: { _ in
            vc.filterContainerHidden = true
        })
    }
    
    @IBAction func applyTapped(_ sender: UIButton) {
        guard let vc = self.parent as? CustomersVC else { return }
        if let statusKey = statusKey {
            vc.filterKey = statusKey
            vc.isFiltering = true
            Customers.searchCustomer(query: emptyString, filter: statusKey) { (result) in
                
                guard let customers = result else { return }
                vc.filteredCustomers.removeAll()
                vc.filteredUsersSections = nil
                for customer in customers {
                    guard let name = customer.customerName, let id = customer.id else { continue }
                    vc.filteredCustomers.append(CustomerName(nameTitle: name, id: id))
                }
                
                let firstLetters = vc.filteredCustomers.map { $0.titleFirstLetter }
                let uniqueFirstLetters = Array(Set(firstLetters))
                
                vc.sortedFirstLettersForFilteredCustomers = uniqueFirstLetters.sorted()
                vc.filteredUsersSections = (vc.sortedFirstLettersForFilteredCustomers?.map { firstLetter in
                    return vc.filteredCustomers
                        .filter { $0.titleFirstLetter == firstLetter }
                        .sorted { $0.nameTitle < $1.nameTitle }
                    })
                
                vc.tableView.reloadData()
            }
        }
        else {
            vc.filterKey = emptyString
            vc.isFiltering = false
            vc.tableView.reloadData()
        }
        closeButton.sendActions(for: .touchUpInside)
    }
    
    @IBAction func resetTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Reset search refinements?", message: "This will reset all refinements but retain your keyword.", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okay = UIAlertAction(title: "Okay", style: .default) { _  in
            self.statusKey = nil
            self.tableView.reloadData()
        }
        alert.view.tintColor = .fieldworkGreen()
        alert.addAction(cancel)
        alert.addAction(okay)
        present(alert, animated: true, completion: nil)
    }
}

extension FiltersVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData[section].expanded ? filterData[section].sectionData.count + 1 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return regularCellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.filterCell) else { return UITableViewCell() }
            cell.textLabel?.text = filterData[indexPath.section].filterTitle
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.filterCell2) else { return UITableViewCell() }
            if filterData[indexPath.section].sectionData[indexPath.row - 1].rawValue.lowercased() == statusKey?.replacingOccurrences(of: "_", with: " ") {
                cell.imageView?.image = #imageLiteral(resourceName: "radioboxMarked")
            }
            else {
                cell.imageView?.image = #imageLiteral(resourceName: "radioboxBlank")
            }
            cell.imageView?.image = cell.imageView?.image?.withRenderingMode(.alwaysTemplate)
            cell.textLabel?.text = filterData[indexPath.section].sectionData[indexPath.row - 1].rawValue
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if filterData[indexPath.section].expanded {
            if indexPath.row == 0 {
                filterData[indexPath.section].expanded = false
            }
            else {
                tableView.cellForRow(at: indexPath)?.imageView?.image = #imageLiteral(resourceName: "radioboxMarked")
                tableView.cellForRow(at: indexPath)?.imageView?.image = tableView.cellForRow(at: indexPath)?.imageView?.image?.withRenderingMode(.alwaysTemplate)
                let statusName = filterData[indexPath.section].sectionData[indexPath.row - 1]
                switch statusName {
                case .active:
                    statusKey = StatusKey.active.rawValue
                case .inactive:
                    statusKey = StatusKey.inactive.rawValue
                case .sendToCollections:
                    statusKey = StatusKey.sendToCollections.rawValue
                case .financialHold:
                    statusKey = StatusKey.financialHold.rawValue
                }
            }
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        else {
            filterData[indexPath.section].expanded = true
            tableView.reloadData()
        }
    }
}
