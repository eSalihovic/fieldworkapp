//
//  CustomerAgreementsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerAgreementsVC: UIViewController {
    
    var agreements: [Agreement]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.title = "Agreements"
        //let rightBarButton = UIBarButtonItem()
        //rightBarButton.image = #imageLiteral(resourceName: "plusIconGreen")
        //navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.customerAgreements,
            let agreementsListVC = segue.destination as? AgreementsListVC else { return }
        agreementsListVC.agreements = agreements
    }
}
