//
//  ServiceLocationsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 08/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

protocol ServiceLocationDelegate: class {
    func updateServiceLocationData(with serviceLocation: [ServiceLocation])
}

class ServiceLocationsVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    weak var delegate: ServiceLocationContainerDelegate?
    
    var serviceLocationData: [ServiceLocation]?
    var searchedServiceLocations = [ServiceLocation]()
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension ServiceLocationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchedServiceLocations.count : serviceLocationData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.serviceLocationCell, for: indexPath)
        cell.backgroundColor = .clear
        if searching {
            cell.textLabel?.text = searchedServiceLocations[indexPath.row].name
            cell.detailTextLabel?.text = createServiceLocationAddress(with: searchedServiceLocations[indexPath.row])
        }
        else {
            cell.textLabel?.text = serviceLocationData?[indexPath.row].name
            cell.detailTextLabel?.text = createServiceLocationAddress(with: serviceLocationData?[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var title = String()
        var description = String()
        if searching {
            title = searchedServiceLocations[indexPath.row].name ?? emptyString
            description = createServiceLocationAddress(with: searchedServiceLocations[indexPath.row])
        }
        else {
            title = serviceLocationData?[indexPath.row].name ?? emptyString
            description = createServiceLocationAddress(with: serviceLocationData?[indexPath.row])
        }
        delegate?.trigerServiceLocationUpdate(with: title, description)
        delegate?.showOrHideServiceLocationContainerView()
    }
}

extension ServiceLocationsVC: ServiceLocationDelegate {
    func updateServiceLocationData(with serviceLocation: [ServiceLocation]) {
        serviceLocationData = serviceLocation
        tableView.reloadData()
    }
}

extension ServiceLocationsVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text != emptyString else {
            searching = false
            tableView.reloadData()
            return
        }
        filterContentForSearchText(text)
        searching = true
        tableView.reloadData()
        textField.resignFirstResponder()
        delegate?.compactServiceLocationContainer()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        tableView.reloadData()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegate?.expandServiceLocationContainer()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        delegate?.compactServiceLocationContainer()
        return true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let serviceLocations = serviceLocationData else { return }
        searchedServiceLocations = serviceLocations.filter({( customer: ServiceLocation) -> Bool in
            return customer.name!.lowercased().contains(searchText.lowercased())
        })
    }
}

extension ServiceLocationsVC {
    
    private func createServiceLocationAddress(with serviceLocation: ServiceLocation?) -> String {
        var addressString = String()
        if let street = serviceLocation?.address?.street, street != emptyString {
            addressString.append(street)
            addressString.append(", ")
        }
        if let city = serviceLocation?.address?.city, city != emptyString {
            addressString.append(city)
            addressString.append(" ")
        }
        if let state = serviceLocation?.address?.state, state != emptyString {
            addressString.append(state)
            addressString.append(" ")
        }
        if let zip = serviceLocation?.address?.zip, zip != emptyString {
            addressString.append(zip)
        }
        
        return addressString
    }
}
