//
//  CustomerDetailsModel.swift
//  FieldWork
//
//  Created by Edin Salihovic on 15/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct CustomerDetails: Decodable {
    let id: Int?
    let accountNumber: Int?
    let namePrefix: String?
    let firstName: String?
    let lastName: String?
    let name: String?
    let customerName: String?
    let customerType: String?
    let site: String?
    let balance: String?
    let invoiceEmail: String?
    let billingPhone: String?
    let billingPhoneKind: String?
    let billingPhones: [String]?
    let billingPhonesKinds: [String]?
    let billingName: String?
    let billingStreet: String?
    let billingCity: String?
    let billingState: String?
    let billingCounty: String?
    let billingZip: String?
    let billingStreet2: String?
    let billingNote: String?
    let sendReportEmail: Bool?
    let emailMarketing: Bool?
    let inspectionsEnabled: Bool?
    let billingTermId: Int?
    let billingAttention: String?
    let lat: String?
    let lng: String?
    let createdAt: String?
    let updatedAt: String?
    let paymentMethods: [PaymentMethod]?
    let serviceLocations: [ServiceLocation]?
    let tasks: [Task]?
    let contacts: [Contact]?
    let invoices: [Invoice]?
    let notes: [Note]?
    let serviceAgreementSetups: [Agreement]?
    let estimates: [Estimate]?
}

struct PaymentMethod: Decodable {
    let name: String?
    let value: String?
}

struct ServiceLocation: Decodable {
    let id: Int?
    let customer_id: Int?
    //let location_type_id: NSNumber?
    //let service_route_id: NSNumber?
    let name: String?
    let email: String?
    let tax_rate_id: Int?
    let hide_balance: Bool?
    let address: Address?
}

struct Address: Decodable {
    let street: String?
    let street2: String?
    let city: String?
    let state: String?
    let zip: String?
    let county: String?
    let notes: String?
    let country: String?
}

struct BillingInfo {
    let address: String?
    let home: String?
    let mobile: String?
    let emai: String?
    let notes: String?
}

struct Task: Codable {
    let id: Int?
    let name: String?
    let status: String?
    let description: String?
    let duration: Int?
    let dueDate: String?
    let calendarDate: String?
    let assignedTo: String?
    let assignedToId: Int?
    let taskRelatableId: Int?
    let taskRelatableType: String?
    let remindMe: String?
    let relatedName: String?
    let taskType: TaskType?
}

struct TaskType: Codable {
    let name: String?
    let createdAt: String?
    let updatedAt: String?
}

struct Contact: Codable {
    //let id: Int?
    let firstName: String?
    let lastName: String?
    let title: String?
    let description: String?
    let phone: String?
    let phoneExt: String?
    let phoneKind: String?
    let phones: [String]?
    let phonesKinds: [String]?
    let phonesExts: [String]?
    let email: String?
    let emailInvoices: Bool?
    let emailWorkOrders: Bool?
}

struct Invoice: Decodable {
    let createdAt: String?
    let approved: Bool?
    let invoiceNumber: Int?
    let status: String?
    let total: String?
    let customerName: String?
    let serviceLocationName: String?
}

struct Note: Codable {
    let attachmentContentType: String?
    let body: String?
    let createdAt: String?
    let userId: Int?
}

struct Agreement: Decodable {
    let id: Int?
    let renews: Bool?
    let createdAt: String?
    let agreementStatus: String?
    let customerName: String?
    let serviceLocationName: String?
}

extension Customers {
    
    class func getCustomerDetails(by id: Int, completion: @escaping (_ result: CustomerDetails?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.customerDetails, id, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode(CustomerDetails.self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
