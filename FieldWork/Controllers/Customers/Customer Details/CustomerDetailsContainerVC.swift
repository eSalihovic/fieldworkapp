//
//  CustomerDetailsContainerVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 08/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

protocol ServiceLocationContainerDelegate: class {
    func showOrHideServiceLocationContainerView()
    func trigerServiceLocationDelegate(with serviceLocation: [ServiceLocation])
    func trigerServiceLocationUpdate(with title: String, _ description: String)
    func expandServiceLocationContainer()
    func compactServiceLocationContainer()
}

class CustomerDetailsContainerVC: UIViewController {
    
    @IBOutlet var serviceLocationContainerView: UIView!
    
    weak var serviceLocationDelegate: ServiceLocationDelegate?
    weak var serviceLocationInfoDelegate: ServiceLocationInfoDelegate?
    
    var customer: CustomerName!
    var serviceLocationContainerHidden = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        setupServiceLocationContainer()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(CustomerDetailsContainerVC.hideServiceLocationContainerView))
        serviceLocationContainerView.addGestureRecognizer(panGesture)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.tintColor = .fieldworkGreen()
        navigationItem.largeTitleDisplayMode = .automatic
    }
    
    private func setupServiceLocationContainer() {
        serviceLocationContainerView.frame = CGRect(x: 0, y: self.view.frame.maxY + 10, width: self.view.frame.size.width, height: self.view.frame.size.height*0.45)
        serviceLocationContainerView.layer.shadowColor = UIColor.black.cgColor
        serviceLocationContainerView.layer.shadowRadius = 5
        serviceLocationContainerView.layer.shadowOpacity = 1
        serviceLocationContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.customerDetails,
            let customerDetailsVC = segue.destination as? CustomerDetailsVC {
            customerDetailsVC.customer = customer
            customerDetailsVC.delegate = self
            serviceLocationInfoDelegate = customerDetailsVC
        }
        else if segue.identifier == Segue.serviceLocation,
            let serviceLocationVC = segue.destination as? ServiceLocationsVC {
            serviceLocationVC.delegate = self
            serviceLocationDelegate = serviceLocationVC
        }
    }
}

extension CustomerDetailsContainerVC: ServiceLocationContainerDelegate {
    
    func trigerServiceLocationUpdate(with title: String, _ description: String) {
        serviceLocationInfoDelegate?.updateServiceLocationInfo(with: title, description)
    }
    
    func trigerServiceLocationDelegate(with serviceLocation: [ServiceLocation]) {
        serviceLocationDelegate?.updateServiceLocationData(with: serviceLocation)
    }
    
    @objc
    func hideServiceLocationContainerView(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: view) // root view
        //serviceLocationContainerView.center = location
        if location.y >= view.frame.maxY*0.25 && location.y <= view.frame.maxY*0.4 {
            compactServiceLocationContainer()
        }
        else if location.y > view.frame.maxY*0.4 && location.y <= view.frame.maxY*0.65 {
            expandServiceLocationContainer()
        }
        else if location.y > view.frame.maxY*0.6 {
            hideServiceLocationContainer()
        }
    }
    
    func expandServiceLocationContainer() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.serviceLocationContainerView.layoutIfNeeded()
            self.serviceLocationContainerView.frame = CGRect(x: 0, y: self.view.frame.maxY*0.25 + 10, width: self.view.frame.size.width, height: self.view.frame.size.height*0.75)
            
        }, completion: { _ in
            self.serviceLocationContainerHidden = false
        })
    }
    
    func compactServiceLocationContainer() {
        showServiceLocationContainer()
    }
    
    func showOrHideServiceLocationContainerView() {
        if serviceLocationContainerHidden {
            showServiceLocationContainer()
        }
        else {
            hideServiceLocationContainer()
        }
    }
    
    private func hideServiceLocationContainer() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.serviceLocationContainerView.layoutIfNeeded()
            self.serviceLocationContainerView.frame = CGRect(x: 0, y: self.view.frame.maxY + 10, width: self.view.frame.size.width, height: self.view.frame.size.height*0.45)
            
        }, completion: { _ in
            self.serviceLocationContainerHidden = true
        })
    }
    
    private func showServiceLocationContainer() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
        self.serviceLocationContainerView.layoutIfNeeded()
        self.serviceLocationContainerView.frame = CGRect(x: 0, y: self.view.frame.maxY*0.55 + 10, width: self.view.frame.size.width, height: self.view.frame.size.height*0.45)
        
        }, completion: { _ in
        self.serviceLocationContainerHidden = false
        })
    }
}
