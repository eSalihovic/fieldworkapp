//
//  CustomerDetailsVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 15/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

protocol ServiceLocationInfoDelegate: class {
    func updateServiceLocationInfo(with title: String, _ description: String)
}

class CustomerDetailsVC: UITableViewController {
    
    @IBOutlet var customerTypeLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var homeLabel: LabelButton!
    @IBOutlet var mobileLabel: LabelButton!
    @IBOutlet var emailLabel: LabelButton!
    @IBOutlet var billingNoteLabel: UILabel!
    @IBOutlet var serviceLocationInfoView: UIView!
    @IBOutlet var serviceLocationTitleLabel: UILabel!
    @IBOutlet var serviceLocationDescLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    @IBOutlet var payNowButton: UIButton!
    @IBOutlet var nextServiceDateLabel: UILabel!
    @IBOutlet var lastModifiedLabel: UILabel!
    
    weak var delegate: ServiceLocationContainerDelegate?
    
    var customer: CustomerName!
    var contacts: [Contact]?
    var tasks: [Task]?
    var invoices: [Invoice]?
    var notes: [Note]?
    var agreements: [Agreement]?
    var estimates: [Estimate]?
    var users: [User]?
    var customerId: Int?
    var collapsedBillingInfo = true
    var locationNameFilterKey = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()  
        
        configureUI()
    }
    
    private func configureUI() {
        serviceLocationInfoView.isHidden = true
        customerTypeLabel.isHidden = true
        customerTypeLabel.layer.cornerRadius = 5
        payNowButton.layer.cornerRadius = 5
        
        homeLabel.onTap = { [weak self] in
            self?.homeLabel.fadeOut(completion: { _ in
                self?.homeLabel.fadeIn()
                self?.homeLabel.text?.makeACall()
            })
        }
        
        mobileLabel.onTap = { [weak self] in
            self?.mobileLabel.fadeOut(completion: { _ in
                self?.mobileLabel.fadeIn()
                self?.mobileLabel.text?.makeACall()
            })
        }
        
        emailLabel.onTap = { [weak self] in
            self?.emailLabel.fadeOut(completion: { _ in
                self?.emailLabel.fadeIn()
                self?.emailLabel.text?.sendEmail()
            })
        }
        
        getDetails(by: customer.id)
    }
    
    @IBAction func searchServiceLocationsTapped(_ sender: UIButton) {
        delegate?.showOrHideServiceLocationContainerView()
    }
    
    @IBAction func hideServiceLocationInfoTapped(_ sender: UIButton) {
        serviceLocationInfoView.isHidden = true
        locationNameFilterKey = emptyString
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .none)
    }
    
    @IBAction func editTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func payNowTapped(_ sender: UIButton) {
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && indexPath.row == 1 {
            return collapsedBillingInfo ? zeroHeight : billingInfoCellHeight
        }
        else if indexPath.section == 0 && indexPath.row == 2 {
            return serviceLocationInfoView.isHidden ? regularCellHeight : 90
        }
        else {
            return regularCellHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 && indexPath.row == 0 {
            collapsedBillingInfo = !collapsedBillingInfo
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        else if indexPath.section == 3 {
            switch indexPath.row {
            case 0:
                //Tasks
                performSegue(withIdentifier: Segue.customerTasksVC, sender: tasks)
                break
            case 1:
                //Contacts
                performSegue(withIdentifier: Segue.customerContactsVC, sender: contacts)
                break
            case 2:
                //Invoices
                guard let invoices = invoices, !invoices.isEmpty else { return }
                var filteredInvoices = [Invoice]()
                for invoice in invoices {
                    guard invoice.serviceLocationName == locationNameFilterKey else { continue }
                    filteredInvoices.append(invoice)
                }
                if locationNameFilterKey != emptyString {
                    performSegue(withIdentifier: Segue.customerInvoicesVC, sender: filteredInvoices)
                }
                else {
                    performSegue(withIdentifier: Segue.customerInvoicesVC, sender: invoices)
                }
                break
            case 3:
                //Notes
                performSegue(withIdentifier: Segue.customerNotesVC, sender: notes)
                break
            case 4:
                //Work order history
                performSegue(withIdentifier: Segue.customerWorkOrderVC, sender: customerId)
                break
            case 5:
                //Agreements
                guard let agreements = agreements, !agreements.isEmpty else { return }
                var filteredAgreements = [Agreement]()
                for agreement in agreements {
                    guard agreement.serviceLocationName == locationNameFilterKey else { continue }
                    filteredAgreements.append(agreement)
                }
                if locationNameFilterKey != emptyString {
                    performSegue(withIdentifier: Segue.customerAgreementsVC, sender: filteredAgreements)
                }
                else {
                    performSegue(withIdentifier: Segue.customerAgreementsVC, sender: agreements)
                }
                break
            case 6:
                performSegue(withIdentifier: Segue.customerEstimatesVC, sender: estimates)
            default:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.customerTasksVC,
            let customerTasksVC = segue.destination as? CustomerTasksVC,
            let tasks = sender as? [Task] {
            customerTasksVC.tasks = tasks
            customerTasksVC.customer = customer
            customerTasksVC.users = users
        }
        else if segue.identifier == Segue.customerContactsVC,
            let customerContactsVC = segue.destination as? CustomerContactsVC,
            let contacts = sender as? [Contact] {
            customerContactsVC.contacts = contacts
            customerContactsVC.customerId = customerId
        }
        else if segue.identifier == Segue.customerNotesVC,
            let customerNotesVC = segue.destination as? CustomerNotesVC,
            let notes = sender as? [Note] {
            customerNotesVC.notes = notes
            customerNotesVC.customerId = customerId
        }
        else if segue.identifier == Segue.customerInvoicesVC,
            let customerInvoicesVC = segue.destination as? CustomerInvoicesVC,
            let invoices = sender as? [Invoice] {
            customerInvoicesVC.invoices = invoices
        }
        else if segue.identifier == Segue.customerWorkOrderVC,
            let customerWorkOrderVC = segue.destination as? CustomerWorkOrderVC,
            let customerId = sender as? Int {
            customerWorkOrderVC.customerId = customerId
            customerWorkOrderVC.locationNameFilterKey = locationNameFilterKey
        }
        else if segue.identifier == Segue.customerAgreementsVC,
            let customerAgreementsVC = segue.destination as? CustomerAgreementsVC,
            let agreements = sender as? [Agreement] {
            customerAgreementsVC.agreements = agreements
        }
        else if segue.identifier == Segue.customerEstimatesVC,
            let customerEstimatesVC = segue.destination as? CustomerEstimatesVC,
            let estimates = sender as? [Estimate] {
            customerEstimatesVC.estimates = estimates
            customerEstimatesVC.title = "Estimates"
        }
    }
}

extension CustomerDetailsVC: ServiceLocationInfoDelegate {
    
    func updateServiceLocationInfo(with title: String, _ description: String) {
        locationNameFilterKey = title
        serviceLocationTitleLabel.text = title
        serviceLocationDescLabel.text = description
        serviceLocationInfoView.isHidden = false
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .none)
    }
}

extension CustomerDetailsVC {
    
    private func getDetails(by id: Int) {
        Spinner.start()
        Customers.getCustomerDetails(by: id) { (customer) in
            guard let customerData = customer else { return }
            self.setupUI(with: customerData)
        }
    }
    
    private func setupUI(with customerData: CustomerDetails) {
        var addressString = String()
        if let billingStreet = customerData.billingStreet, billingStreet != emptyString {
            addressString.append(billingStreet)
            addressString.append(" ")
        }
        if let billingStreet2 = customerData.billingStreet2, billingStreet2 != emptyString {
            addressString.append(billingStreet2)
            addressString.append(" ")
        }
        if let billingCity = customerData.billingCity, billingCity != emptyString {
            addressString.append(billingCity)
            addressString.append(" ")
        }
        if let billingState = customerData.billingState, billingState != emptyString {
            addressString.append(billingState)
            addressString.append(", ")
        }
        if let billingZip = customerData.billingZip, billingZip != emptyString {
            addressString.append(billingZip)
        }
        
        if addressString.isEmpty {
            addressLabel.text = noValueString
        }
        else {
           addressLabel.text = addressString
        }
        
        nextServiceDateLabel.text = noValueString
        mobileLabel.text = noValueString
        homeLabel.text = noValueString
        mobileLabel.textColor = .black
        homeLabel.textColor = .black
        
        if let phones = customerData.billingPhones, let kinds = customerData.billingPhonesKinds {
            //Highly NOT recomended way to extract values but API is terrible
            
            if let phone = customerData.billingPhone, let kind = customerData.billingPhoneKind {
                if kind.lowercased() == mobile && phone != emptyString {
                    mobileLabel.text = phone
                    mobileLabel.textColor = .fieldworkGreen()
                }
                else if kind.lowercased() == home && phone != emptyString {
                    homeLabel.text = phone
                    homeLabel.textColor = .fieldworkGreen()
                }
            }
            
            if phones.count == kinds.count {
                for (i, kind) in kinds.enumerated() {
                    if kind.lowercased() == mobile && phones[i] != emptyString {
                        mobileLabel.text = phones[i]
                        mobileLabel.textColor = .fieldworkGreen()
                    }
                    else if kind.lowercased() == home && phones[i] != emptyString {
                        homeLabel.text = phones[i]
                        homeLabel.textColor = .fieldworkGreen()
                    }
                }
            }
        }
        
        if let email = customerData.invoiceEmail, email != emptyString {
            emailLabel.text = email
            emailLabel.textColor = .fieldworkGreen()
        }
        else {
            emailLabel.text = noValueString
            emailLabel.textColor = .black
        }
        
        if let billingNote = customerData.billingNote, billingNote != emptyString {
            billingNoteLabel.text = billingNote
        }
        else {
            billingNoteLabel.text = noValueString
        }
        
        if let balance = customerData.balance, balance != emptyString {
            balanceLabel.text = "$" + balance
        }
        else {
            balanceLabel.text = noValueString
        }
        
        if let customerType = customerData.customerType, customerType != emptyString {
            customerTypeLabel.text = customerType
            customerTypeLabel.isHidden = false
        }
        else {
            customerTypeLabel.isHidden = true
        }
        
        if let lastModified = customerData.updatedAt, lastModified != emptyString {
            if let dateString = lastModified.convertStringDateToString() {
                lastModifiedLabel.text = "Last modified " + dateString
            }
            else {
                lastModifiedLabel.text = noValueString
            }
        }
        
        contacts = customerData.contacts
        tasks = customerData.tasks
        invoices = customerData.invoices
        notes = customerData.notes
        agreements = customerData.serviceAgreementSetups
        estimates = customerData.estimates
        customerId = customerData.id
        
        if let serviceLocation = customerData.serviceLocations {
            delegate?.trigerServiceLocationDelegate(with: serviceLocation)
        }
        
        Users.getUsers { (users) in
            self.users = users
        }
        
        Spinner.stop()
    }
}
