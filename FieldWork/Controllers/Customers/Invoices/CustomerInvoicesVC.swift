//
//  CustomerInvoicesVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 19/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerInvoicesVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var invoices: [Invoice]?
    var searchedInvoices = [Invoice]()
    var sorted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.setupSearchController(with: self)
        setupNavBar()
        guard let invoices = invoices, invoices.isEmpty else { return }
        tableView.isHidden = true
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    @IBAction func sortTapped(_ sender: UIBarButtonItem) {
        var comparator: (Int, Int) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        invoices = invoices?.sorted(by: { comparator($0.invoiceNumber ?? 0, $1.invoiceNumber ?? 0) })
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
}

extension CustomerInvoicesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching() ? searchedInvoices.count : invoices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.invoiceTableViewCell, for: indexPath) as? InvoiceTableViewCell else { return UITableViewCell() }
        
        var approved = false
        var invoiceNumberString = String()
        var customerName = String()
        var serviceLocation = String()
        var priceString = String()
        var dateString = String()
        var status = String()
        
        if isSearching() {
            approved = searchedInvoices[indexPath.row].approved ?? false
            invoiceNumberString = "# \(searchedInvoices[indexPath.row].invoiceNumber ?? 0)"
            customerName = searchedInvoices[indexPath.row].customerName ?? emptyString
            serviceLocation = searchedInvoices[indexPath.row].serviceLocationName ?? emptyString
            priceString = "$"
            priceString.append(searchedInvoices[indexPath.row].total ?? emptyString)
            dateString = searchedInvoices[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
            status = searchedInvoices[indexPath.row].status?.capitalized ?? emptyString
        }
        else {
            //DO NOT LEAVE IT THIS WAY
            approved = invoices?[indexPath.row].approved ?? false
            invoiceNumberString = "# \(invoices?[indexPath.row].invoiceNumber ?? 0)"
            customerName = invoices?[indexPath.row].customerName ?? emptyString
            serviceLocation = invoices?[indexPath.row].serviceLocationName ?? emptyString
            priceString = "$"
            priceString.append(invoices?[indexPath.row].total ?? emptyString)
            dateString = invoices?[indexPath.row].createdAt?.convertStringDateToString() ?? emptyString
            status = invoices?[indexPath.row].status?.capitalized ?? emptyString
        }
        
        cell.titleLabel.text = invoiceNumberString
        cell.locationLabel.text = serviceLocation
        cell.customerLabel.text = customerName
        cell.priceLabel.text = priceString
        cell.dateLabel.text = dateString
        cell.statusLabel.text = status
        cell.isApprovedImageView.isHidden = !approved

        if status.lowercased() == InvoiceStatus.paid {
            cell.statusLabel.textColor = .fieldworkGreen()
            cell.statusView.backgroundColor = .fieldworkGreen()
        }
        else if status.lowercased() == InvoiceStatus.unpaid {
            cell.statusLabel.textColor = .fieldworkGray()
            cell.statusView.backgroundColor = .fieldworkGray()
        }
        else if status.lowercased() == InvoiceStatus.overdue {
            cell.statusLabel.textColor = .fieldworkRed()
            cell.statusView.backgroundColor = .fieldworkRed()
        }
        else if status.lowercased() == InvoiceStatus.badDebt {
            cell.statusLabel.textColor = .fieldworkGray()
            cell.statusView.backgroundColor = .fieldworkGray()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CustomerInvoicesVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        filterContentForSearchText(text)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let invoices = invoices else { return }
        searchedInvoices = invoices.filter({( invoice: Invoice) -> Bool in
            let invoicesNrString = String(invoice.invoiceNumber!)
            return invoicesNrString.contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}
