//
//  InvoicesTableViewCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 22/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {
    
    @IBOutlet var statusView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var customerLabel: UILabel!    
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var isApprovedImageView: UIImageView!
    @IBOutlet var mapMarkerIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statusView.backgroundColor = .clear
        statusView.layer.cornerRadius = statusView.frame.height / 2
        titleLabel.text = emptyString
        locationLabel.text = emptyString
        customerLabel.text = emptyString
        priceLabel.text = emptyString
        statusLabel.text = emptyString
        dateLabel.text = emptyString
    }
}
