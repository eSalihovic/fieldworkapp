//
//  CustomersVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 11/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomersVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var filterContainerView: UIView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var sortedFirstLettersForAllCustomers: [String]?
    var sortedFirstLettersForFilteredCustomers: [String]?
    var allUsersSections: [[CustomerName]]?
    var filteredUsersSections: [[CustomerName]]?
    var customerNames = [CustomerName]()
    var searchedCustomers = [CustomerName]()
    var filteredCustomers = [CustomerName]()
    var currentPage = 1
    var filterKey = emptyString
    var filterContainerHidden = true
    var isFiltering = false
    var isFromAddTask = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        searchController.setupSearchController(with: self)
        searchController.searchBar.delegate = self
        Spinner.start()
        setupNavBar()
        setupFilterContainer()
        
        getCustomers(on: currentPage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segue.customerDetailsContainer,
            let customerDetailsContainerVC = segue.destination as? CustomerDetailsContainerVC,
            let customer = sender as? CustomerName else { return }
        customerDetailsContainerVC.title = customer.nameTitle
        customerDetailsContainerVC.customer = customer
    }
    
    // TODO: make nav. extension
    private func setupNavBar() {
        //self.navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.searchController = searchController
        definesPresentationContext = true
        if isFromAddTask {
            navigationItem.leftBarButtonItem = nil
            navigationItem.hidesBackButton = false
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = .zero
    }
    
    @IBAction func sideMenuTapped(_ sender: UIBarButtonItem) {
        perform(#selector(ESSideMenu.presentLeftMenuViewController))
    }
    
    private func setupFilterContainer() {
        filterContainerView.frame = CGRect(x: self.view.frame.maxX, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        filterContainerView.layer.cornerRadius = 10
    }
    
    @IBAction func filterTapped(_ sender: UIButton) {
        if filterContainerHidden {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.filterContainerView.layoutIfNeeded()
                self.filterContainerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                
            }, completion: { _ in
                self.filterContainerHidden = false
            })
        }
        else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                self.filterContainerView.layoutIfNeeded()
                self.filterContainerView.frame = CGRect(x: self.view.frame.maxX, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                
            }, completion: { _ in
                self.filterContainerHidden = true
            })
        }
        //UIApplication.shared.keyWindow?.addSubview(filterContainerView)
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        // TODO
    }
}

extension CustomersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let filteredCount = isFiltering ? filteredUsersSections?[section].count ?? 0 : allUsersSections?[section].count ?? 0
        return isSearching() ? searchedCustomers.count : filteredCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearching() {
            return emptyString
        }
        else if isFiltering {
            return sortedFirstLettersForFilteredCustomers?[section] ?? emptyString
        }
        else {
            return sortedFirstLettersForAllCustomers?[section] ?? emptyString
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering {
            return sortedFirstLettersForFilteredCustomers
        }
        else {
            return sortedFirstLettersForAllCustomers
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching() {
            return 1
        }
        else if isFiltering {
            return filteredUsersSections?.count ?? 0
        }
        else {
            return allUsersSections?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return regularCellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        guard let sections = allUsersSections else { return UITableViewCell() }
        if isSearching() {
            cell.textLabel?.text = searchedCustomers[indexPath.row].nameTitle
        }
        else if isFiltering {
            let customerName = filteredUsersSections?[indexPath.section][indexPath.row]
            cell.textLabel?.text = customerName?.nameTitle
        }
        else {
            let customerName = sections[indexPath.section][indexPath.row]
            cell.textLabel?.text = customerName.nameTitle
        }
        
        if indexPath.section == sections.count - 1 {
            if currentPage != 0 {
                currentPage += 1
                getCustomers(on: currentPage)
            }
        }
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isSearching() {
            guard !searchedCustomers.isEmpty else { return }
            let customerName = searchedCustomers[indexPath.row]
            goToNextVC(customer: customerName)
        }
        else if isFiltering {
            guard let customerName = filteredUsersSections?[indexPath.section][indexPath.row] else { return }
            goToNextVC(customer: customerName)
        }
        else {
            guard let customerName = allUsersSections?[indexPath.section][indexPath.row] else { return }
            goToNextVC(customer: customerName)
        }
    }
}

extension CustomersVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, text != emptyString else { return }
        filterContentForSearchText(text)
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = emptyString
        tableView.reloadData()
    }
}

extension CustomersVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        // Do nothing...
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        Spinner.start()
        Customers.searchCustomer(query: searchText, filter: filterKey) { (result) in
            
            guard let customers = result else { return }
            self.searchedCustomers.removeAll()
            for customer in customers {
                guard let name = customer.customerName, let id = customer.id else { continue }
                self.searchedCustomers.append(CustomerName(nameTitle: name, id: id))
            }
            
            self.tableView.reloadData()
            Spinner.stop()
        }
    }
}

extension CustomersVC {
    
    func goToNextVC(customer: CustomerName) {
        if !isFromAddTask {
            performSegue(withIdentifier: Segue.customerDetailsContainer, sender: customer)
        }
        else {
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
                let addOrEditTaskVC = self.navigationController?.viewControllers.last as? AddOrEditTaskVC
                addOrEditTaskVC?.customer = customer
                addOrEditTaskVC?.customerTF.text = customer.nameTitle
            }
        }
    }
    
    private func getCustomers(on page: Int) {
        if let apiKey = UserDefaults.standard.string(forKey: "APIKey") {
            Spinner.start()
            Customers.getCustomers(by: page, apiKey: apiKey) { (result) in
                
                guard let customers = result else { return }
                if customers.count < perPage {
                    self.currentPage = 0
                }
                
                for customer in customers {
                    guard let name = customer.customerName, let id = customer.id else { continue }
                    self.customerNames.append(CustomerName(nameTitle: name, id: id))
                }
            
                let firstLetters = self.customerNames.map { $0.titleFirstLetter }
                let uniqueFirstLetters = Array(Set(firstLetters))

                self.sortedFirstLettersForAllCustomers = uniqueFirstLetters.sorted()
                self.allUsersSections = (self.sortedFirstLettersForAllCustomers?.map { firstLetter in
                    return self.customerNames
                        .filter { $0.titleFirstLetter == firstLetter }
                        .sorted { $0.nameTitle < $1.nameTitle }
                })
                
                self.tableView.reloadData()
                Spinner.stop()
            }
        }
        else {
            sideMenuViewController?.contentViewController = .instantiateSignInVC()
        }
    }
}
