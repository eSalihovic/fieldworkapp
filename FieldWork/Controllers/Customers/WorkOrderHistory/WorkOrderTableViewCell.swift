//
//  WorkOrderTableViewCell.swift
//  FieldWork
//
//  Created by Edin Salihovic on 27/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class WorkOrderTableViewCell: UITableViewCell {
    
    @IBOutlet var reportNumberLabel: UILabel!
    @IBOutlet var serviceLocationLabel: UILabel!
    @IBOutlet var technicianNameLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var statusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statusView.backgroundColor = .clear
        statusView.layer.cornerRadius = statusView.frame.height / 2
        reportNumberLabel.text = emptyString
        serviceLocationLabel.text = emptyString
        technicianNameLabel.text = emptyString
        statusLabel.text = emptyString
        statusLabel.textColor = .fieldworkGray()
        dateLabel.text = emptyString
    }
}
