//
//  CustomerWorkOrderVC.swift
//  FieldWork
//
//  Created by Edin Salihovic on 27/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

class CustomerWorkOrderVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var workOrders: [WorkOrder]?
    var searchedWorkOrders = [WorkOrder]()
    var statuses = [WorkOrderStatus]()
    var searching = false
    var sorted = false
    var customerId: Int?
    var locationNameFilterKey: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let customerId = customerId else { return }
        getWorkOrders(by: customerId)
    }
    
    private func setupNavBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }
    
    @IBAction func sortTapped(_ sender: UIButton) {
        var comparator: (Int, Int) -> Bool = (<)
        
        if sorted {
            comparator = (>)
            sorted = false
        }
        else {
            comparator = (<)
            sorted = true
        }
        
        workOrders = workOrders?.sorted(by: { comparator($0.reportNumber ?? 0, $1.reportNumber ?? 0) })
        tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .bottom)
    }
}

extension CustomerWorkOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchedWorkOrders.count : workOrders?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.workOrderTableViewCell, for: indexPath) as? WorkOrderTableViewCell else { return UITableViewCell() }
        
        var workOrderNumberString = String()
        var serviceLocation = String()
        var technicianName = String()
        var statusString = String()
        var dateString = String()
        
        if searching {
            workOrderNumberString = "# \(searchedWorkOrders[indexPath.row].reportNumber ?? 0)"
            technicianName = searchedWorkOrders[indexPath.row].technicianName ?? noValueString
            serviceLocation = searchedWorkOrders[indexPath.row].serviceLocationName ?? noValueString
            statusString = searchedWorkOrders[indexPath.row].status?.capitalized ?? noValueString
            dateString = searchedWorkOrders[indexPath.row].startsAt?.convertStringDateToString() ?? noValueString
        }
        else {
            //DO NOT LEAVE IT THIS WAY
            workOrderNumberString = "# \(workOrders?[indexPath.row].reportNumber ?? 0)"
            technicianName = workOrders?[indexPath.row].technicianName ?? noValueString
            serviceLocation = workOrders?[indexPath.row].serviceLocationName ?? noValueString
            statusString = workOrders?[indexPath.row].status?.capitalized ?? noValueString
            dateString = workOrders?[indexPath.row].startsAt?.convertStringDateToString() ?? noValueString
        }
        
        cell.reportNumberLabel.text = workOrderNumberString
        cell.serviceLocationLabel.text = serviceLocation
        cell.technicianNameLabel.text = technicianName
        cell.statusLabel.text = statusString
        cell.dateLabel.text = dateString
        cell.statusView.backgroundColor = .lightGray
        
        for status in statuses {
            guard statusString.lowercased() == status.value?.lowercased() else { continue }
            guard let hexColorString = status.color else { continue }
            cell.statusView.backgroundColor = .hexStringToUIColor(hex: hexColorString)
            cell.statusLabel.textColor = .hexStringToUIColor(hex: hexColorString)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension CustomerWorkOrderVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text != emptyString else {
            searching = false
            tableView.reloadData()
            return
        }
        filterContentForSearchText(text)
        searching = true
        tableView.reloadData()
        textField.resignFirstResponder()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searching = false
        tableView.reloadData()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        guard let workOrders = workOrders else { return }
        searchedWorkOrders = workOrders.filter({( workOrder: WorkOrder) -> Bool in
            let workOrderNrString = String(workOrder.reportNumber!)
            return workOrderNrString.contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}

extension CustomerWorkOrderVC {
    
    func getWorkOrders(by customerId: Int) {
        Spinner.start()
        WorkOrders.getWorkOrders(by: customerId) { (result) in
            guard let workOrders = result else {
                Spinner.stop()
                self.tableView.isHidden = true
                return
            }
            
            if let filterKey = self.locationNameFilterKey, filterKey != emptyString {
                var filteredWorkOrders = [WorkOrder]()
                for workOrder in workOrders {
                    guard workOrder.serviceLocationName == filterKey else { continue }
                    filteredWorkOrders.append(workOrder)
                }
                self.workOrders = filteredWorkOrders
            }
            else {
                self.workOrders = workOrders
            }
            
            if let orders = self.workOrders, orders.count == 0 {
                self.tableView.isHidden = true
            }
            WorkOrders.getStatuses(completion: { (statuses) in
                guard let statuses = statuses else { return }
                self.statuses = statuses
                self.tableView.reloadData()
                Spinner.stop()
            })
            self.tableView.reloadData()
            Spinner.stop()
        }
    }
}
