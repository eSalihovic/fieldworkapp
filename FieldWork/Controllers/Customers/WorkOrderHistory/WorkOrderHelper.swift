//
//  WorkOrderHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 28/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct WorkOrder: Decodable {
    let reportNumber: Int?
    let startsAt: String?
    let technicianName: String?
    let status: String?
    let serviceLocationName: String?
}

struct WorkOrderStatus: Decodable {
    let name: String?
    let value: String?
    let color: String?
    let fontColor: String?
}

class WorkOrders {
    
    class func getWorkOrders(by customerId: Int, completion: @escaping (_ result: [WorkOrder]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.workOrderHistory, customerId, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([WorkOrder].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
    
    class func getStatuses(completion: @escaping (_ result: [WorkOrderStatus]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.statuses, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([WorkOrderStatus].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}

