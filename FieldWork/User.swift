//
//  User.swift
//  FieldWork
//
//  Created by Edin Salihovic on 20/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: Int?
    let firstName: String?
    let lastName: String?
}

class Users {
    
    class func getUsers(completion: @escaping (_ result: [User]?) -> Void) {
        guard let apiKey = UserDefaults.standard.string(forKey: "APIKey") else { return }
        // generate the url string
        let apiUrl = String(format: Endpoint.users, apiKey)
        
        ApiService.getData(from: apiUrl) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let apiResponse = try decoder.decode([User].self, from: data)
                completion(apiResponse)
            } catch let jsonError {
                print("Error serializing json", jsonError)
                completion(nil)
            }
        }
    }
}
