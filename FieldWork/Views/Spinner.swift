//
//  Spinner.swift
//  FieldWork
//
//  Created by Edin Salihovic on 13/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

open class Spinner {
    
    internal static var spinner: UIActivityIndicatorView?
    
    open static var style: UIActivityIndicatorViewStyle = .whiteLarge
    open static var baseBackColor = UIColor(white: 0, alpha: 0.1)
    open static var baseColor: UIColor = .fieldworkGreen()
    
    open static func start(style: UIActivityIndicatorViewStyle = style, backColor: UIColor = baseBackColor, baseColor: UIColor = baseColor) {
        if spinner == nil, let window = UIApplication.shared.keyWindow {
            let frame = UIScreen.main.bounds
            spinner = UIActivityIndicatorView(frame: frame)
            spinner!.backgroundColor = backColor
            spinner!.activityIndicatorViewStyle = style
            spinner?.color = baseColor
            window.addSubview(spinner!)
            spinner!.startAnimating()
        }
    }
    
    open static func stop() {
        if spinner != nil {
            spinner!.stopAnimating()
            spinner!.removeFromSuperview()
            spinner = nil
        }
    }
    
    open static func update() {
        if spinner != nil {
            stop()
            start()
        }
    }
}
