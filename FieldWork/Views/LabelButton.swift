//
//  LabelButton.swift
//  FieldWork
//
//  Created by Edin Salihovic on 05/07/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class LabelButton: UILabel {
    
    var onTap: () -> Void = {}
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onTap()
    }
}
