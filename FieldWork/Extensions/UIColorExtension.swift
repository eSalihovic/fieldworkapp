//
//  UIColorExtension.swift
//  FieldWork
//
//  Created by Edin Salihovic on 5/7/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func fieldworkGreen() -> UIColor {
        return UIColor(red: 26/255, green: 104/255, blue: 78/255, alpha: 1)
    }
    
    class func fieldworkGray() -> UIColor {
        return UIColor(red: 83/255, green: 89/255, blue: 97/255, alpha: 1)
    }
    
    class func fieldworkPurple() -> UIColor {
        return UIColor(red: 111/255, green: 103/255, blue: 193/255, alpha: 1)
    }
    
    class func fieldworkRed() -> UIColor {
        return UIColor(red: 239/255, green: 83/255, blue: 80/255, alpha: 1)
    }
    
    class func fieldworkYelow() -> UIColor {
        return UIColor(red: 233/255, green: 207/255, blue: 90/255, alpha: 1)
    }
    
    class func pendingTaskStatus() -> UIColor {
        return UIColor(red: 111/255, green: 103/255, blue: 193/255, alpha: 1)
    }
    
    class func waitingTaskStatus() -> UIColor {
        return UIColor(red: 51/255, green: 159/255, blue: 172/255, alpha: 1)
    }
    
    class func completeTaskStatus() -> UIColor {
        return UIColor(red: 51/255, green: 172/255, blue: 132/255, alpha: 1)
    }
    
    class func inProgressTaskStatus() -> UIColor {
        return UIColor(red: 248/255, green: 231/255, blue: 28/255, alpha: 1)
    }
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
