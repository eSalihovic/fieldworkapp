//
//  StringExtension.swift
//  FieldWork
//
//  Created by Edin Salihovic on 21/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func convertStringDateToString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateFromString = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        guard let date = dateFromString else { return nil }
        return dateFormatter.string(from: date)
    }
    
    func timeFromDateStringToString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateFromString = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "HH:mm"
        guard let time = dateFromString else { return nil }
        return dateFormatter.string(from: time)
    }
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
        case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        guard isValid(regex: .phone), let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
    
    func sendEmail() {
        guard isValid(regex: .email), let url = URL(string: "mailto:\(self)"), UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
}
