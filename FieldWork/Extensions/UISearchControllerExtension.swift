//
//  UISearchControllerExtension.swift
//  FieldWork
//
//  Created by Edin Salihovic on 21/06/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import Foundation

extension UISearchController {
    
    func setupSearchController(with delegate: UISearchResultsUpdating) {
        self.searchResultsUpdater = delegate
        self.searchBar.tintColor = .fieldworkGreen()
        self.obscuresBackgroundDuringPresentation = false
    }
}
