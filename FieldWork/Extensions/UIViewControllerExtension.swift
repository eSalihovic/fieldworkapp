//
//  UIViewControllerExtension.swift
//  FieldWork
//
//  Created by Edin Salihovic on 23/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func instantiateSignInVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.signIn, bundle: nil)
        guard let signInVC = storyboard.instantiateViewController(withIdentifier: ViewController.signIn) as? SignInVC else {
            return UIViewController()
        }
        
        return signInVC
    }
    
    class func instantiateDashboardVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.main, bundle: nil)
        guard let dashboardVC = storyboard.instantiateViewController(withIdentifier: ViewController.dashboard) as? DashboardVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: dashboardVC)
        return naviController
    }
    
    class func instantiateCustomersVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.customers, bundle: nil)
        guard let customersVC = storyboard.instantiateViewController(withIdentifier: ViewController.customers) as? CustomersVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: customersVC)
        return naviController
    }
    
    class func instantiateCalendarVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.calendar, bundle: nil)
        guard let calendarVC = storyboard.instantiateViewController(withIdentifier: ViewController.calendaar) as? CalendarVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: calendarVC)
        return naviController
    }
    
    class func instantiateTasksVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.tasks, bundle: nil)
        guard let tasksVC = storyboard.instantiateViewController(withIdentifier: ViewController.tasks) as? TasksVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: tasksVC)
        return naviController
    }
    
    class func instantiateEstimatesVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.estimates, bundle: nil)
        guard let estimatesVC = storyboard.instantiateViewController(withIdentifier: ViewController.estimates) as? EstimatesVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: estimatesVC)
        return naviController
    }
    
    class func instantiateAgreementsVC() -> UIViewController {
        let storyboard = UIStoryboard(name: Storyboard.agreements, bundle: nil)
        guard let agreementsVC = storyboard.instantiateViewController(withIdentifier: ViewController.agreements) as? AgreementsVC else {
            return UIViewController()
        }
        
        let naviController = UINavigationController(rootViewController: agreementsVC)
        return naviController
    }
    
    var previousViewController: UIViewController? {
        if let controllersOnNavStack = self.navigationController?.viewControllers {
            let n = controllersOnNavStack.count
            //if self is still on Navigation stack
            if controllersOnNavStack.last === self, n > 1 {
                return controllersOnNavStack[n - 2]
            }
            else if n > 0 {
                return controllersOnNavStack[n - 1]
            }
        }
        return nil
    }
}
