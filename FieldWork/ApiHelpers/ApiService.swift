//
//  ApiHelper.swift
//  FieldWork
//
//  Created by Edin Salihovic on 11/05/2018.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import UIKit
import Foundation

class ApiService {
    
    class func getData(from urlString: String, completion: @escaping (_ result: Data?) -> Void) {
        // the URL
        guard let url = URL(string: urlString) else { return }
        print("url = \(url)")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // the request object
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 100
        
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                print("""
                    API request failed: \(String(describing: request.url))
                    error = \(error.debugDescription)
                    """)
                
                completion(nil)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return
            }
            
            completion(data)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        })
        task.resume()
    }
    
    class func postData(with request: URLRequest, completion: @escaping (_ result: Data?) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Create and run a URLSession data task with our JSON encoded POST request
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("""
                    API request failed: \(String(describing: request.url))
                    error = \(error.debugDescription)
                    """)
                
                completion(nil)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return
            }
            
            // APIs usually respond with the data you just sent in your POST request
            completion(data)
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        
        task.resume()
    }
    
    // encode get data
    class func encodeGetData(_ dictionary: [AnyHashable: Any]) -> String {
        
        var parts = [String]()
        for item in dictionary {
            if let key = item.key as? String {
                guard let value = dictionary[key] as? String else { continue }
                let encodedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let encodedKey = key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                parts.append(encodedKey! + "=" + encodedValue!)
            }
        }
        return "?\(parts.joined(separator: "&"))"
    }
    
    // encode dictionary into a string
    class func encodePostData(_ dictionary: [AnyHashable: Any]) -> Data {
        var parts = [String]()
        for item in dictionary {
            if let key = item.key as? String {
                guard let value = dictionary[key] as? String else { continue }
                let encodedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let encodedKey = (key as NSString).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let part = encodedKey! + "=" + encodedValue!
                parts.append(part)
            }
        }
        let encodedDictionary: String = parts.joined(separator: "&")
        return encodedDictionary.data(using: String.Encoding.utf8) ?? Data()
    }
}
