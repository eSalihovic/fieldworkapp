//
//  Constants.swift
//  FieldWork
//
//  Created by Edin Salihovic on 5/7/18.
//  Copyright © 2018 qsdbih. All rights reserved.
//

import Foundation
import UIKit

let emptyString = ""
let noValueString = "N/A"
let mobile = "mobile"
let home = "home"
let contactName = "%@, %@"
let regularCellHeight: CGFloat = 55
let billingInfoCellHeight: CGFloat = 270
let zeroHeight: CGFloat = 0
let perPage = 50

struct Endpoint {
    static let productionServer     = "https://api.fieldworkhq.com/v3/"
    static let testServer           = "https://api.fieldworkapp.com/v3/"
    static let login                = productionServer + "get_api_key"
    static let users                = productionServer + "users?api_key=%@"
    static let customers            = productionServer + "customers?page=%d&per_page=%d&api_key=%@"
    static let customerDetails      = productionServer + "customers/%d?api_key=%@"
    static let searchCustomers      = productionServer + "customers/search?query=%@&filter[customer_status]=%@&api_key=%@"
    static let tasks                = productionServer + "tasks?page=%d&per_page=%d&api_key=%@"
    static let submitTask           = productionServer + "tasks?api_key=%@"
    static let agreements           = productionServer + "service_agreement_setups?&api_key=%@"
    static let workOrderHistory     = productionServer + "work_order_history?customer_id=%d&api_key=%@"
    static let statuses             = productionServer + "statuses?api_key=%@"
    static let estimates            = productionServer + "estimates?page=%d&per_page=%d&api_key=%@"
    static let submitContact        = productionServer + "customers/%d/contacts?api_key=%@"
    static let submitNote           = productionServer + "customers/%d/notes?api_key=%@"
    static let submitTaskFilters    = productionServer + "tasks?filter[status]=%@filter[task_type]=%@filter[assigned_to]=%@filter[due_date]=%@filter[calendar_date]=%@api_key=%@"
}

struct LeftMenu {
    static let titles = ["Dashboard", "Customers", "Calendar", "Tasks", "Estimates", "Agreements", "Settings", "Logout"]
    static let images = ["dashboardLeftMenuIcon", "customersLeftMenuIcon", "calendarLeftMenuIcon", "tasksLeftMenuIcon", "estimatesLeftMenuIcon", "agreementsLeftMenuIcon", "settingsLeftMenuIcon", "logoutLeftMenuIcon"]
}

struct Storyboard {
    static let main                     = "Main"
    static let signIn                   = "SignIn"
    static let customers                = "Customers"
    static let calendar                 = "Calendar"
    static let tasks                    = "Tasks"
    static let estimates                = "Estimates"
    static let agreements               = "Agreements"
}

struct ViewController {
    static let signIn                   = "SignInVC"
    static let leftMenu                 = "LeftMenuVC"
    static let dashboard                = "DashboardVC"
    static let customers                = "CustomersVC"
    static let customerDetails          = "CustomerDetailsVC"
    static let calendaar                = "CalendarVC"
    static let tasks                    = "TasksVC"
    static let estimates                = "EstimatesVC"
    static let agreements               = "AgreementsVC"
}

struct Segue {
    static let customerDetailsContainer = "customerDetailsContainer"
    static let customerDetails          = "customerDetails"
    static let serviceLocation          = "serviceLocation"
    static let customerTasksVC          = "customerTasksVC"
    static let customerContactsVC       = "customerContactsVC"
    static let customerTasks            = "customerTasks"
    static let addOrEditTask            = "addOrEditTask"
    static let taskListVC               = "taskListVC"
    static let customerNotesVC          = "customerNotesVC"
    static let customerInvoicesVC       = "customerInvoicesVC"
    static let customerAgreementsVC     = "customerAgreementsVC"
    static let customerAgreements       = "customerAgreements"
    static let agreementsListVC         = "agreementsListVC"
    static let customerWorkOrderVC      = "customerWorkOrderVC"
    static let addOrEditContact         = "addOrEditContact"
    static let addOrEditNoteVC          = "addOrEditNoteVC"
    static let taskDetailsVC            = "taskDetailsVC"
    static let tasksFilterVC            = "tasksFilterVC"
    static let tasksFilterDateCell      = "tasksFilterDateCell"
    static let customerEstimatesVC      = "customerEstimatesVC"
    static let estimatesVC              = "estimatesVC"
    static let customersVC              = "customersVC"
}

struct ReuseIdentifier {
    static let leftMenuCell             = "leftMenuCell"
    static let taskTableViewCell        = "taskTableViewCell"
    static let filterCell               = "filterCell"
    static let filterCell2              = "filterCell2"
    static let serviceLocationCell      = "serviceLocationCell"
    static let contactCell              = "contactCell"
    static let noteTableViewCell        = "noteTableViewCell"
    static let invoiceTableViewCell     = "invoiceTableViewCell"
    static let agreementTableViewCell   = "agreementTableViewCell"
    static let workOrderTableViewCell   = "workOrderTableViewCell"
    static let estimateTableViewCell    = "estimateTableViewCell"
    static let contactPhoneCell         = "contactPhoneCell"
    static let taskInfoCell             = "taskInfoCell"
    static let relatedToCell            = "relatedToCell"
    static let scheduleCell             = "scheduleCell"
    static let tasksFilterCell          = "tasksFilterCell"
    static let tasksFilterDateCell      = "tasksFilterDateCell"
}

struct TaskStatus {
    static let complete                 = "complete"
    static let pending                  = "pending"
    static let inProgress               = "in progress"
    static let waitingForReply          = "waiting for reply"
}

struct InvoiceStatus {
    static let paid                     = "paid"
    static let unpaid                   = "unpaid"
    static let overdue                  = "overdue"
    static let badDebt                  = "bad debt"
}

struct AgreementStatus {
    static let active                   = "active"
    static let hold                     = "hold"
    static let pending                  = "pending"
    static let canceled                 = "cancelled"
    static let expired                  = "expired"
}
